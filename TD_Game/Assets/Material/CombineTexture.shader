Shader "Unlit/CombineTexture"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)
        _TexSecond ("Texture", 2D) = "white" {}
        _Color2("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        Cull front

        Pass
        {

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float4 _Color;
            float4 _Color2;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _TexSecond;
            float4 _TexSecond_ST;

            v2f vert (appdata input)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(input.vertex);
                o.uv = input.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                float4 col = _Color;
                float4 col2 = _Color2;
                float4 texMain = tex2D(_MainTex, i.uv);
                float4 texTwo = tex2D(_TexSecond, i.uv);


                return (col * texMain) + ( col2 * texTwo);
            }
            ENDCG
        }
    }
}
