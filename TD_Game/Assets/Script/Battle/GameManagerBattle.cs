using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManagerBattle : MonoBehaviour
{
    public static GameManagerBattle instance;

    [Header("Setting Battle: ")]
    public int totTeam = 5;
    public int totShop = 3;
    public int startElisir = 5;
    public int addAllTurnElisir = 6;
    public int MaxElisir = 10;
    public int MaxTroop = 8;
    public int startShuffle = 2;
    public int maxShuffle = 6;
    public int addAllTurnShuffle = 2;
    public int CostMaxTroop = 9;
    private int speedTimeBattle = 1;
    public float TimeGame = 1;
    public TypeGame typeGame;
    public TeamManager tm;
    [Header("Setting: ")]
    [SerializeField] private float waitStartPreparationTroop;
    [Header("Object Ref: ")]
    public GameObject objectHeroInGame;

    public bool finishBattle;

    #region Events

    public UnityEvent startPraparetionHero;
    public void InvokStartPreparetionHero() => startPraparetionHero.Invoke();

    public UnityEvent startBattle;
    public void InvokStartBattle() => startBattle.Invoke();

    public UnityEvent<int> endTurn;
    public void InvokeEndTurnBattle(int team) =>endTurn.Invoke(team);

    public UnityEvent controlBattle;
    public void InvokeControlBattle() => controlBattle.Invoke();

    public UnityEvent troopPreparetionGrid;

    public void InvokeTroopPreparetionGrid() => troopPreparetionGrid.Invoke();

    public UnityEvent<int> finishGame;
    public void InvokeFinishGame(int teamID) => finishGame.Invoke(teamID);
    
    public UnityEvent<int> ChangeTimerBattle;
    public void InvokeChangeTimerBattle(int timer) => ChangeTimerBattle?.Invoke(timer);

    #endregion

    private void Awake()
    {
        instance = this;
        startBattle.AddListener(() => StartCoroutine(SetTimeBattle(speedTimeBattle, 3)));
        startPraparetionHero.AddListener(() => { typeGame = TypeGame.Preparetion; });
        startPraparetionHero.AddListener(() => StartCoroutine(SetTimeBattle(1)));
        troopPreparetionGrid.AddListener(() => { typeGame = TypeGame.Preparetion; });
        troopPreparetionGrid.AddListener(() => StartCoroutine(SetTimeBattle(1)));
        ChangeTimerBattle.AddListener((timer) => StartCoroutine(SetTimeBattle(timer, 2, true)));
        tm.Setup();
        StartCoroutine(SetTimeBattle(1, 0, true));
    }

    public void StartGame()
    {
        InvokStartPreparetionHero();
    }

    public IEnumerator SetTimeBattle(int timer, int durationChange = 0, bool changeTimer = false)
    {
        if(changeTimer)
            speedTimeBattle = timer;

        float timeStart = 0;
        float time = Time.timeScale;

        while (timeStart < durationChange)
        {
            float t = timeStart / durationChange;
            Time.timeScale = Mathf.Lerp(time, timer, t);
            TimeGame = Time.timeScale;
            timeStart += Time.deltaTime;
            yield return null;
        }
        
        Time.timeScale = timer;
    }
    public void StartBattleInvoke()
    {
        foreach (Squad squad in tm.squads.Keys)
        {
            if (squad.GetFinish())
            {
                continue;
            }

            return;
        }

        

        if (typeGame != TypeGame.Battle)
        {
            typeGame = TypeGame.Battle;
            finishBattle = false;
            InvokStartBattle();
        }
        
    }

    public List<HeroMonster> GetAllTypeHeroInGame(int team)
    {
        List<HeroMonster> heroTot = new List<HeroMonster>();

        foreach (HeroMonster hero in GetComponentsInChildren<HeroMonster>())
        {
            if (hero.GetTeam().GetTeamId() != team)
            {
                heroTot.Add(hero);
            }
        }

        return heroTot;
    }

    public void EndGame(int team)
    {
        if (!ControlEnemyInGame(team) && !finishBattle)
        {
            StartCoroutine(SetTimeBattle(1));
            InvokeEndTurnBattle(ReversControl(team));
            finishBattle = true;
            Debug.Log("Finish");
        }
    }
    public bool ControlEnemyInGame(int team)
    {
        foreach (HeroMonster hero in GetComponentsInChildren<HeroMonster>())
        {
            if (hero.GetTeam().GetTeamId() == team && !hero.death.GetDeath())
            {
                return true;
            }
        }

        return false;
    }

    private int ReversControl(int team)
    {
        if (team == 1)
            return 2;
        if (team == 2)
            return 1;

        return 0;
    }
}

[System.Serializable]

public enum TypeGame
{
    Preparetion,
    Battle
}