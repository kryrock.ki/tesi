using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

[System.Serializable]
public class TeamManager
{
    public Dictionary<Squad, TypeHeroControl> squads = new Dictionary<Squad, TypeHeroControl>();
    //public List<Squad> list;
    public Color TeamColor1 = Color.cyan;
    public Color TeamColor2 = Color.red;
    public bool isPlayer;
    public void Setup()
    {
        foreach (Squad squad in GameObject.FindObjectsOfType<Squad>()) 
        {
            if (squad.typeControl == TypeHeroControl.Player)
            {
                if (!squads.ContainsValue(TypeHeroControl.Player))
                {
                    squads.Add(squad, TypeHeroControl.Player);
                    isPlayer = true;
                }
                else
                {
                    squads.Add(squad, TypeHeroControl.CPU);
                    squad.typeControl = TypeHeroControl.CPU;
                }

            }
            else
            {
                squads.Add(squad, TypeHeroControl.CPU);
                squad.typeControl = TypeHeroControl.CPU;
            }
            
        }

        //list = new List<Squad>(squads.Keys);

        SetTeam();
    }

    public Squad GetTeam(int teamID)
    {
        foreach(Squad squad in squads.Keys)
        {
            if (squad.GetTeamId() == teamID)
            {
                return squad;
            }
        }

        return null;
    }

    private void SetTeam()
    {
        int index = 1;

        if(squads.ContainsValue(TypeHeroControl.Player))
            index = 2;
        
        foreach (Squad squad in squads.Keys)
        {
            if (squad)
            {
                if(squad.typeControl == TypeHeroControl.Player)
                    squad.SettingTeam(1, SetColor(index));
                else
                {
                    squad.SettingTeam(index, SetColor(index));
                    index++;
                }
               
            }

        }
    }

    public Squad GetPlayerTeam()
    {
        foreach (Squad squad in squads.Keys)
        {
            if (squad)
            {
                if (squad.typeControl == TypeHeroControl.Player)
                    return squad;
            }

        }

        return null;
    }

    private Color SetColor(int team)
    {
        if(team % 2 == 0)
        {
            return TeamColor2;
        }
        else
        { return TeamColor1; }
    }
}
