using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerBattleSingle : GameManagerBattle
{
    public Point points;

    private void Start()
    {
        endTurn.AddListener(SetPoint);
        controlBattle.AddListener(StartControlBattle);
    }

    private void SetPoint(int team)
    {
        if(team % 2 == 0)
        {
            points.AddPointEnemy();
        }
        else
        {
            points.AddPointPlayer();
        }
    }

    private void StartControlBattle()
    {
        finishBattle = false;

        Debug.Log("StartControl");
        
        if (points.GetPointPlayer() >= points.GetMaxPoint())
        {
            InvokeFinishGame(1);
            return;
        }
        if (points.GetPointEnemy() >= points.GetMaxPoint())
        {
            InvokeFinishGame(2);
            return;
        }

        InvokeTroopPreparetionGrid();
    }
}
