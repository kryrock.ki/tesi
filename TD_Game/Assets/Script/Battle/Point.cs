using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Point
{
    [SerializeField] private int pointPlayer;
    public int GetPointPlayer() { return pointPlayer; }
    [SerializeField] private int pointEnemy;
    public int GetPointEnemy() { return pointEnemy; }
    [SerializeField] private int MaxPoint = 3;
    public int GetMaxPoint() { return MaxPoint; }
    public int AddPointPlayer()
    {
       return pointPlayer++;
    }

    public int AddPointEnemy()
    {
        return pointEnemy++;
    }

    public void ResetPoint()
    {
        pointPlayer = 0;
        pointEnemy = 0;
    }
}
