using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using static UnityEngine.GraphicsBuffer;
using UnityEngine.Events;

[System.Serializable]
public class Moved
{
    public HeroMonster hero;
    public bool moving;
    public TileGrid destination;
    private Pathfind path;


    #region Event

    public UnityAction endMoving;
    public void InvokeEndMoving() => endMoving?.Invoke();
    #endregion

    public void Setup(HeroMonster hero)
    {
        this.hero = hero;
        path = new Pathfind();

        hero.death.deathEvent.AddListener(Reset);
    }

    public void SetupBattle()
    {
        moving = false;
        
    }

    private void Reset()
    {
        moving = false;
        hero.animationHero.BoolAnimation("Run", false);
        if(destination != null && destination != hero.tileGridCurrent)
        {
            destination.SetOccupied(false, null);
            destination = null;
        }
    }

    public void Moving(HeroMonster target, TileGrid currentTile)
    {

        if (target == null)
            return;
        
        if (!moving)
        {

            destination = null;
            //List<TileGrid> candidates = new List<TileGrid>(target.tileGridCurrent.GetNeighborn());
            //candidates.AddRange(target.tileGridCurrent.GetNeighbornDiagonal());
            //
            //
            //candidates = candidates.OrderBy(x => Vector3.Distance(x.transform.position, hero.gameObject.transform.position)).ToList();
            //
            //for (int i = 0; i < candidates.Count; i++)
            //{
            //    if (!candidates[i].GetOccupated())
            //    {
            //        destination = candidates[i];
            //        break;
            //    }
            //}
            //
            //if (destination == null)
            //    return;
            //
            //var path = this.path.GetShortestPath(currentTile, destination, hero);
            //
            //tile = path.ToArray();
            //
            //if (path == null || path.Count < 2)
            //    return;

            List<TileGrid> path = GetTileNeighborFree(currentTile, target.tileGridCurrent);


            if (path == null || path.Count < 2)
                return;

            if (path[1].GetOccupated() || hero.tileGridCurrent == path[1])
                return;



            destination = path[1];
            destination.SetOccupied(true, hero);

            


        }

        if (destination != null)
        {

            moving = !MoveTowards(destination);
            
            
            if (!moving)
            {
                //Free previous node
                Debug.Log("Move");
                currentTile.SetOccupied(false, null);
                hero.SetTile(destination);
                hero.SetBattleState(BattleStateHero.None);
                InvokeEndMoving();
            }
        }

    }

    private bool MoveTowards(TileGrid nextNode)
    {
        if(nextNode == null)
            return false;

        Vector3 direction = (nextNode.transform.position - hero.gameObject.transform.position);       
        if (direction.sqrMagnitude <= (0.1f * Time.timeScale))
        {
            hero.gameObject.transform.position = nextNode.transform.position;
            hero.animationHero.BoolAnimation("Run", false);
            return true;
        }
        hero.animationHero.BoolAnimation("Run", true);
        hero.SetBattleState(BattleStateHero.Move);
        
        hero.gameObject.transform.position += direction.normalized * hero.info.GetMovimentSpeed() * Time.deltaTime;
        Quaternion rotation = Quaternion.LookRotation(nextNode.transform.position - hero.gameObject.transform.position);
        
        hero.transform.rotation = Quaternion.Slerp(hero.transform.rotation, rotation, Time.deltaTime * 20);
        return false;
    }

    private List<TileGrid> GetTileNeighborFree(TileGrid startTile, TileGrid tileTarget)
    {
        TileGrid start = startTile;
        TileGrid end = tileTarget;

        List<TileGrid> path = new List<TileGrid>();

        while (true)
        {

            if(end == startTile) break;

            List<TileGrid> candidates = new List<TileGrid>(end.GetNeighborn());
            candidates.AddRange(end.GetNeighbornDiagonal());


            candidates = candidates.OrderBy(x => Vector3.Distance(x.transform.position, hero.gameObject.transform.position)).ToList();

            for (int i = 0; i < candidates.Count; i++)
            {
                if (!candidates[i].GetOccupated())
                {
                    destination = candidates[i];
                    break;
                }
                else if (candidates[i].GetHero() == hero)
                {
                    destination = candidates[i];
                    break;
                }
            }

            if (destination == null)
                return null;

            path = this.path.GetShortestPath(startTile, destination, hero);


           


            if (path == null || path.Count < 2)
            {
                end = candidates[0];

                continue;
            }

            break;
        }

        return path;
    }
}


