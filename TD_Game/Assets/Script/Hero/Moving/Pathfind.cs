using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Pathfind
{
    public List<TileGrid> GetShortestPath(TileGrid start, TileGrid end, HeroMonster hero)
    {
        List<TileGrid> path = new List<TileGrid>();

        // se il tile iniziale � uguale al tile finale, restituisce il tile iniziale
        if (start == end)
        {
            path.Add(start);
            return path;
        }

        // lista dei tile vicini vuoti
        List<TileGrid> neighbor = new List<TileGrid>();
        List<TileGrid> diagonal = new List<TileGrid>();
        // tile precedenti nel percorso ottimale dall'origine
        Dictionary<TileGrid, TileGrid> previous = new Dictionary<TileGrid, TileGrid>();

        // Le distanze calcolate, impostate tutte su Infinito, tranne il tile di partenza
        Dictionary<TileGrid, float> distances = new Dictionary<TileGrid, float>();

        for (int i = 0; i < GridManager.instance.tileGrieds.Count; i++)
        {
            TileGrid node = GridManager.instance.tileGrieds[i];

            neighbor.Add(node);

            distances.Add(node, float.MaxValue);
            // Impostazione della distanza del nodo su Infinity
        }

        // Impostare la distanza del nodo iniziale su zero
        distances[start] = 0f;
        while (neighbor.Count != 0)
        {
            // Ottenere il nodo con la distanza pi� piccola
            neighbor = neighbor.OrderBy(node => distances[node]).ToList();
            TileGrid current = neighbor[0];

            neighbor.Remove(current);

            if (current.GetOccupated() && current.GetHero() != hero)
                continue;


            // Quando il nodo corrente � uguale al nodo finale, allora possiamo interrompere e restituire il percorso
            if (current == end)
            {
                // Costruisco il percorso pi� breve
                while (previous.ContainsKey(current))
                {
                    // Inserisci il nodo sul risultato finale
                    path.Insert(0, current);
                    // Attraversa dall'inizio alla fine
                    current = previous[current];
                }

                //Inserisci la fonte nel risultato finale
                path.Insert(0, current);
                break;
            }

            // loop attraverso i tile vicini e dove i tile (vicino) � disponibile nell'elenco non visitato
            foreach (TileGrid neighborTile in current.GetNeighborn())
            {
                // Ottenere la distanza tra il tile corrente e il tile vicino
                float length = Vector3.Distance(current.transform.position, neighborTile.transform.position);

                // La distanza del tile iniziale al tile vicino del tile corrente
                float alt = distances[current] + length;

                // trovare un percorso pi� breve per il tile vicino.
                if (alt < distances[neighborTile] && !neighborTile.GetOccupated())
                {
                    distances[neighborTile] = alt;
                    previous[neighborTile] = current;
                }
                else if(!neighborTile.GetOccupated())
                {
                
                    diagonal = neighborTile.GetNeighbornDiagonal().ToList();
                
                    if (diagonal.Contains(neighborTile))
                    {
                        distances[neighborTile] = alt;
                        previous[neighborTile] = current;
                    }
                }
            }
        }
        return path;
    }

   
}
