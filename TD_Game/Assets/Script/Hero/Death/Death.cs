using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Death
{
    public bool death;

    #region Event

    public UnityEvent deathEvent;
    public void InvokeDeath() => deathEvent?.Invoke();

    #endregion



    public bool GetDeath()
    {
        return death;
    }
    private HeroMonster hero;
    public void Setup(HeroMonster hero)
    {
        this.hero = hero;
        death = false;
    }

    public void Reset()
    {
        death = false;
    }

    public void SetDead(bool value)
    {
        

        if (!death)
            hero.StartCoroutine(Dead());
        
        death = true;

        hero.condition.RemoveCondition();
    }

    private IEnumerator Dead()
    {
        InvokeDeath();
        hero.stateHero = BattleStateHero.None;
        hero.animationHero.GetAnimation().speed = 1;
        hero.animationHero.BoolAnimation("Dead", true);
        yield return new WaitForSeconds(0.2f);
        //yield return new WaitForSeconds(hero.animationHero.GetDurationTransition());
        hero.condition.EndConditionInvok();
        hero.tileGridCurrent.SetOccupied(false, null); 
        yield return new WaitForSeconds(2f);
        hero.animationHero.BoolAnimation("Dead", false);
        Debug.Log("Deadddd");
        hero.SetMeshRender(false, false);
       
    }
}
