using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.Rendering;



[RequireComponent(typeof(HeroScript))]
public class SelectionHero : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler, /*IDropHandler,*/ IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{

    #region Variable

    [Header("Drag: ")]
    [SerializeField] private LayerMask layerMaskDrag;
    [Header("Drop: ")]
    [SerializeField] private LayerMask layerMaskDrop;
    [Header("Selection: ")]
    [SerializeField] private float speedRotation = 50;
    [SerializeField] private float traslateShopY = 5;
    [SerializeField] private float traslateDragY = 1;
    [SerializeField] private Color selectionColorOutline;
    [SerializeField] private Color notSelectionColorOutline;
    [SerializeField] private Color SelectionFullColorOutline;
    [Header("Object Ref: ")]
    [SerializeField] private GameObject heroObject;
    [SerializeField] private GameObject piedistallo;
    [SerializeField] private SkinnedMeshRenderer mesh;

    private HeroMonster hero;
    private SortingGroup sortingGroup;

    public bool select;
    public bool isPlace;
    public bool Drag;
    public bool isPossibleClick;
    private float halfScreen => (Screen.safeArea.height / 4);

    private TileGrid tileSelect;

    #endregion

    public void Start()
    {
        hero = GetComponent<HeroMonster>();
        //mesh = GetComponentInChildren<SkinnedMeshRenderer>();
        sortingGroup = GetComponent<SortingGroup>();

        if(hero.info.GetRarity() == Rarity.Hero)
        {
            isPossibleClick = true;
            
        }

        //GridManager.instance.clearTileEvent += () => PossibleClickDamage(false);
        PlayerAction.Instance.clickHeroEvent += (hero) => PossibleClickDamage(hero);
        UIManagerBattle.instance.GetUIInfoMonster().startTimerEvent += () => isPossibleClick = false;
        
        ChangeOutline(notSelectionColorOutline);
    }

    public void ActivatedPlatform(bool active)
    {
        piedistallo.SetActive(active);
    }

    public void Update()
    {
        if (select && !Drag && !isPlace)
        {
            heroObject.transform.Rotate(new Vector3(0, speedRotation, 0) * Time.deltaTime);
        }


    }

    public void SelectionAction(bool select, bool drag, bool place)
    {
        this.select = select;
        Drag = drag;
        isPlace = place;

    }
    private void ChangeOutline(Color color)
    {
        foreach (Material mt in mesh.materials)
        {
            mt.SetColor("_OutColor", color);
        }
        
        piedistallo.GetComponentInChildren<MeshRenderer>().material.SetColor("_OutColor", color);

    }

    private RaycastHit RaycastOnGame(LayerMask layerMask)
    {
        Ray raycastMouse = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit raycastHit;

        Physics.Raycast(raycastMouse, out raycastHit, 1000, layerMask);


        return raycastHit;
    }

    private Vector3 RaycastOnOutGame()
    {

        Vector2 pos;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
        UIManagerBattle.instance.GetUIShop().transform as RectTransform, Input.mousePosition,
        Camera.main,
        out pos);

        return new Vector3(pos.x, pos.y, -400);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!select && !Drag && !isPlace)
        {
            if (!GridManager.instance.ControlTileEmpty(hero.GetTeam().GetTeamId()))
            {
                ChangeOutline(SelectionFullColorOutline);
                return;
            }
            if (hero.info.GetInfoHero().GetCost() > hero.GetTeam().elisir || !hero.GetTeam().PossibleAddTroop())
            {
                ChangeOutline(SelectionFullColorOutline);
                return;
            }

            heroObject.transform.localPosition += new Vector3(0, 1, 0) * traslateShopY;
            ChangeOutline(selectionColorOutline);
            SelectionAction(true, false, false);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isPlace && !Drag)
        {
            SetHeroLocalPositionAndRotation(Vector3.zero, Vector3.zero);
            ChangeOutline(notSelectionColorOutline);
            SelectionAction(false, Drag, isPlace);
        }

        if (UIManagerBattle.instance.GetUIInfoMonster().GetActivatedTimer())
            ActiveTimer(false);

    }

    private void SetHeroLocalPositionAndRotation(Vector3 position, Vector3 eulerAngles)
    {
        heroObject.transform.localPosition = position;
        heroObject.transform.localEulerAngles = eulerAngles;
    }

    public void OnDrag(PointerEventData eventData)
    {

        if (!select)
            return;

        ActiveTimer(false);

        SelectionAction(true, true, false);

        sortingGroup.sortingOrder = 1;

        transform.SetParent(GameManagerBattle.instance.objectHeroInGame.transform);

        SetHeroLocalPositionAndRotation(Vector3.zero, Vector3.zero);
        transform.rotation = new Quaternion(0, 0, 0, 0);
        
        ActivatedPlatform(true);

        if (!IsOfScreenLimit())
        {
            RaycastHit hit = RaycastOnGame(layerMaskDrag);
            FindTileSelection(hit);
            transform.position = hit.point + (Vector3.up * traslateDragY);
        }
        else
            transform.localPosition = RaycastOnOutGame();


        ChangeOutline(selectionColorOutline);

        PlayerAction.Instance.DragHeroInvok(hero.TypeControl, hero);

    }

    //public void OnDrop(PointerEventData eventData)
    //{
    //    
    //
    //    //if (!Drag)
    //    //    return;
    //    //
    //    //isPlace = false;
    //    //
    //    //TileGrid tile = null;
    //    //
    //    //if (tileSelect)
    //    //{
    //    //    tile = tileSelect;
    //    //
    //    //    tileSelect.SelectTile(false);
    //    //    tileSelect = null;
    //    //
    //    //    isPlace = true;
    //    //}
    //    //
    //    //SelectionAction(true, false, isPlace);
    //    //
    //    //PlayerAction.DropHeroEvent(isPlace, tile, hero);
    //    //
    //    //ChangeOutline(notSelectionColorOutline);
    //    //
    //    //sortingGroup.sortingOrder = 0;
    //}

    private bool IsOfScreenLimit()
    {
        if (Input.mousePosition.y < halfScreen)
        {
            transform.SetParent(UIManagerBattle.instance.GetUIShop().transform);

            if (hero.info.GetRarity() == Rarity.Hero)
                return true;


            if (tileSelect)
                tileSelect.SelectTile(TileSelection.None);
            tileSelect = null;
            GridManager.instance.SetAllSelectionTile(TileSelection.None);
            //transform.position = new Vector3(transform.position.x, transform.position.y, 10);
            return true;
        }

        return false;
    }

    private void FindTileSelection(RaycastHit hit)
    {

        float distance = Mathf.Infinity;
        TileGrid correctTile = tileSelect;

        TileGrid tileHit = null;

        if (hit.collider.gameObject.layer == 9)
            tileHit = hit.collider.GetComponent<TileGrid>();

        if (tileHit == null || !tileHit.CorrectTile(hero.GetTeam().GetTeamId()))
        {
            foreach (TileGrid tile in GridManager.instance.tileGrieds)
            {
                if (tile.CorrectTile(hero.GetTeam().GetTeamId()))
                {
                    float dst = Vector3.Distance(hit.point, tile.transform.position);
                    if (dst < distance)
                    {
                        tileSelect = tile;

                        distance = dst;
                    }
                }

            }
        }
        else
            tileSelect = tileHit;

        if (correctTile == tileSelect)
            return;

        GridManager.instance.SetAllSelectionTile(TileSelection.None);

        if (correctTile)
            correctTile.SelectTile(TileSelection.None);

        tileSelect.SelectTile(TileSelection.Select);

        foreach (TileGrid tile in hero.TilesRange(tileSelect, hero.attack.attackBase))
        {
            tile.SelectTile(TileSelection.Damage);
        }

        hero.attack.ViewAttackZone(tileSelect, AttackMove.Primary, TileSelection.PrimaryDamage);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (GameManagerBattle.instance.typeGame != TypeGame.Battle)
            ActiveTimer(true);


    }

    private void ActiveTimer(bool activatedInfo = false)
    {
        UIManagerBattle.instance.ActiveInfoMonster(activatedInfo, hero);

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (UIManagerBattle.instance.GetUIInfoMonster().GetActivatedTimer())
            ActiveTimer(false);

        if (!Drag)
            return;

        isPlace = false;

        TileGrid tile = null;

        if (tileSelect)
        {
            tile = tileSelect;

            tileSelect.SelectTile(TileSelection.None);
            tileSelect = null;

            isPlace = true;
            isPossibleClick = false;
        }

        //SelectionAction(true, false, isPlace);
        //
        //PlayerAction.DropHeroEvent(isPlace, tile, hero);

        ChangeOutline(notSelectionColorOutline);

        sortingGroup.sortingOrder = 0;

        PlayerAction.Instance.DropHeroEvent(isPlace, tile, hero);

        GridManager.instance.SetAllSelectionTile(TileSelection.None);

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GridManager.instance.SetAllSelectionTile(TileSelection.None);

        PlayerAction.Instance.ClickHeroInvok(hero);
    }

    private bool PossibleClickDamage(HeroMonster hero)
    {

        if (GameManagerBattle.instance.typeGame == TypeGame.Battle)
            return false;

        if (this.hero.isGame && isPossibleClick && hero == this.hero)
        {
            foreach (TileGrid tile in hero.TilesRange(hero.tileGridCurrent, hero.attack.attackBase))
            {
                tile.SelectTile(TileSelection.Damage);
            }

            hero.attack.ViewAttackZone(hero.tileGridCurrent, AttackMove.Primary, TileSelection.PrimaryDamage);

            isPossibleClick = false;

            return true;
        }
        else if ( !isPossibleClick || hero != this.hero)
        {
            if(this.hero.isGame)
                isPossibleClick = true;

            return false;
        }

        return false;
    }
}
