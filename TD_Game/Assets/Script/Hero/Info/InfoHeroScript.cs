using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class InfoHeroScript
{
    [SerializeField] private Rarity rarity;
    public Rarity GetRarity() => rarity;
    [SerializeField] private int healtMax;
    public int GetHealtMax() => healtMax;
    [SerializeField] private int healtCurrent;
    public int GetHealtCurrent() => healtCurrent;
    [SerializeField] private float superCurrent = 0;
    public float GetSuperCurrent() => superCurrent;
    [SerializeField] private HeroObject infoHero;
    public HeroObject GetHeroObject() => infoHero;
    public InfoHero GetInfoHero() => infoHero.info;

    [SerializeField] private int level;
    public int GetLevel() => level;
    [SerializeField] private float movimentSpeed;
    public float GetMovimentSpeed() => movimentSpeed;

    #region Event
    public UnityAction superEvent;
    public void SuperInvok() => superEvent.Invoke();
    #endregion


    public void Setup()
    {
        rarity = infoHero.GetRarity();
        healtMax = infoHero.info.GetHealtMax();
        healtCurrent = healtMax;
        level = infoHero.GetLevel() - 1;
        movimentSpeed = infoHero.info.GetMovingSpeed();
        superCurrent = 0;
        
    }

    public InfoAttack GetAttackBaseInfo()
    {
        return infoHero.info.AttackBase.GetAttack(level);
    }

    public InfoAttack GetAttackPrimaryInfo()
    {
        if(infoHero.info.AttackPrimary)
            return infoHero.info.AttackPrimary.GetAttack(level);
        return null;
    }

    public InfoAttack[] GetAttackSpecialInfo()
    {
        List<InfoAttack> attack = new List<InfoAttack>();
        
        foreach (AttackBase att in infoHero.info.AttackSpecial)
        {
            attack.Add(att.GetAttack(level));
        }


        return attack.ToArray();
    }

    public bool isSuper()
    {
        if (superCurrent >= infoHero.info.GetMaxSuper())
            return true;

        return false;
    }

    public int SetHealtCurrent(int damage)
    {

        healtCurrent -= damage;

        return healtCurrent;
    }

    public void SuperActive(bool damage = false)
    {
        if(GetAttackSpecialInfo().Length < 1)
            return;
    
        if (damage)
        {
            superCurrent += infoHero.info.GetDamageIncrementSuper();
        }
        else
        {
            superCurrent += infoHero.info.GetIncrementSuper();
        }

        superCurrent = Mathf.Clamp(superCurrent, 0, infoHero.info.GetMaxSuper());

        SuperInvok();
    }

    public void ResetSuper()
    {
        if (superCurrent >= infoHero.info.GetMaxSuper())
            superCurrent = 0;

        SuperInvok();
    }

}
