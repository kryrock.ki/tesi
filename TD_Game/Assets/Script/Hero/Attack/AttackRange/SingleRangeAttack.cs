using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleRangeAttack : Attack
{
    public SingleRangeAttack(HeroMonster hero, InfoAttack attack, string animationName)
    {
        heroSelf = hero;
        FindHeroType(attack.GetTypeTarget());
        attackInfo = attack;    

        this.animationName = animationName;
    }

    public override void ActionAttack(HeroMonster target, int damage)
    {
        if (target)
        {
            heroSelf.tileGridCurrent.GetConditionParticle().ActiveParticle(typeEffectTileHero);

            if (objectPool != null)
            {
                GameObject bullet = objectPool.Get();
                bullet.GetComponent<RangeBullet>().Setup(heroSelf,damage, attackInfo, target.gameObject);
            }

            SetCondition(new HeroMonster[] { target} , new HeroMonster[] { heroSelf });

        }
        
    }

    public override TileGrid[] GetAllTileTarget(TileGrid tileGrid)
    {
        return null;
    }
}
