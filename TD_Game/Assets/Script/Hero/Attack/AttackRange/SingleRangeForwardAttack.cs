using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleRangeForwardAttack : Attack
{
    public SingleRangeForwardAttack(HeroMonster hero, InfoAttack attack, string animationName)
    {
        heroSelf = hero;
        FindHeroType(attack.GetTypeTarget());
        attackInfo = attack;    

        this.animationName = animationName;
    }

    public override void ActionAttack(HeroMonster target, int damage)
    {
        heroSelf.tileGridCurrent.GetConditionParticle().ActiveParticle(typeEffectTileHero);

        TileGrid[] tiles = GetAllTileTarget(heroSelf.tileGridCurrent);

        foreach(TileGrid tile in tiles)
        {
            if(tile.GetHero() != null && tile.GetHero().GetTeam().GetTeamId() != heroSelf.GetTeam().GetTeamId())
            {
                if (objectPool != null)
                {
                    GameObject bullet = objectPool.Get();
                    bullet.GetComponent<RangeBullet>().Setup(heroSelf, damage, attackInfo, tile.GetHero().gameObject);
                }
        
                SetCondition(new HeroMonster[] { tile.GetHero() }, new HeroMonster[] { heroSelf });
                
                return;
            }
        }

        if (objectPool != null && tiles.Length > 0)
        {
            GameObject bullet = objectPool.Get();
            bullet.GetComponent<RangeBullet>().Setup(heroSelf, damage, attackInfo, tiles[tiles.Length - 1].gameObject);
        }

        SetCondition(null, new HeroMonster[] { heroSelf });


    }

    public override TileGrid[] GetAllTileTarget(TileGrid tileGrid)
    {
        return GetTileForward(tileGrid);
    }

    private TileGrid[] GetTileForward(TileGrid currentTile)
    {
        List<TileGrid> tiles = new List<TileGrid>();

        int range = 0;

        Vector3 forward = heroSelf.transform.forward;

        Vector3 tileposition = currentTile.transform.localPosition;

        while (range < attackInfo.GetRangeAttack())
        {
            Vector3 tilePos = new Vector3(tileposition.x, tileposition.y, tileposition.z + ((forward.z * (range + 1)) * GridManager.instance.GetSpaceTile()));
            TileGrid tile = GridManager.instance.GetTileBasedonPosition(tilePos);

            if (tile != null)
            {
                tiles.Add(tile);
            }

            range++;



        }
        
        return tiles.ToArray();
    }
}
