using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultyRangeAttack : Attack
{
    public MultyRangeAttack(HeroMonster hero, InfoAttack attack, string animationName)
    {
        heroSelf = hero;
        FindHeroType(attack.GetTypeTarget());
        attackInfo = attack;

        this.animationName = animationName;
    }

    public override void ActionAttack(HeroMonster target, int damage)
    {
        if (target)
        {
            heroSelf.tileGridCurrent.GetConditionParticle().ActiveParticle(typeEffectTileHero);
            List<HeroMonster> enemys = new List<HeroMonster>();
            List<HeroMonster> player = new List<HeroMonster>();
            enemys.Add(target);
            player.Add(heroSelf);

            for (int i = 0; i < attackInfo.GetRangeAttack(); i++)
            {
                if (objectPool != null)
                {
                    
                    if (i == 0)
                    {
                        GameObject bullet = objectPool.Get();
                        bullet.GetComponent<RangeBullet>().Setup(heroSelf, damage, attackInfo, target.gameObject);
                    }
                    else
                    {
                        FindHeroScript findHero = new FindHeroScript(attackInfo.GetTypeTarget());
                        HeroMonster multyTarget = findHero.findHeroEnemy.FindTarget(heroSelf, enemys);
                        if (multyTarget)
                        {
                            GameObject bullet = objectPool.Get();
                            bullet.GetComponent<RangeBullet>().Setup(heroSelf, damage, attackInfo, multyTarget.gameObject);
                            enemys.Add(multyTarget);
                        }

                    }

                }

            }

            SetCondition(enemys.ToArray(), player.ToArray());
        }
        
    }

    public override TileGrid[] GetAllTileTarget(TileGrid tileGrid)
    {
        return null;
    }
}
