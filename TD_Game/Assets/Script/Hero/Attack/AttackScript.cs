using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackScript : MonoBehaviour
{
    public bool canAttack = true;
    public bool haveAttack = false;
    private int attackSuperIndex;
    public Attack attackPrimary;
    public Attack attackBase;
    public Attack[] attackSpecial;
    public Attack selectionAttack;
    public HeroMonster hero;
    public bool activePrimaryAttack;


    // Start is called before the first frame update
    void Awake()
    {
        hero = GetComponentInParent<HeroMonster>();
        attackSpecial = new Attack[hero.info.GetInfoHero().AttackSpecial.Length];
        SetAttackbase();
    
    }

    public void SetupBattle()
    {
        canAttack = true;
        haveAttack = false;
        activePrimaryAttack = false;
    }

    public void AttackEffectAnimation()
    {
        if (!hero.death.GetDeath())
        {
            int damage = selectionAttack.attackInfo.GetDamage();
            
            if (hero.condition.GetConditionActive())
            {
                damage += (int)hero.condition.GetCondition().ActiveConditionDamageBoostPercent(damage);
            }
            
            selectionAttack.ActionAttack(hero.target,  damage);
            
            if (selectionAttack.animationName.Contains("AttackSpecial"))
                hero.info.ResetSuper();
        }
        
    }

    private void SetAttackbase()
    {
        attackBase = SetTypeAttack(hero.info.GetInfoHero().AttackBase, "AttackBase");
        
        for (int i = 0; i < hero.info.GetInfoHero().AttackSpecial.Length; i++)
        {
            attackSpecial[i] = SetTypeAttack(hero.info.GetInfoHero().AttackSpecial[i], "AttackSpecial");
        }
        
        attackPrimary = SetTypeAttack(hero.info.GetInfoHero().AttackPrimary, "AttackPrimary");

    }

    private Attack SetTypeAttack(AttackBase attackBase, string animationName)
    {
        if (attackBase == null)
            return null;

        Attack attackDate = null;
        int level = hero.info.GetLevel();


        switch (attackBase.GetAttack(level).GetTypeAttack())
        {
            case TypeAttack.Single:
                attackDate = new SingleMeeleAttack(hero, attackBase.GetAttack(level), animationName);
                break;
            case TypeAttack.CaselleAttornoARombo:
                attackDate = new AoERomboMeeleAttack(hero, attackBase.GetAttack(level), animationName); 
                break;
            case TypeAttack.TreCaselleAvantiASe:
                attackDate = new AoETreCaselleForwardMeeleAttack(hero, attackBase.GetAttack(level), animationName);
                break;
            case TypeAttack.CaselleAttornoAQuadrato:
                attackDate = new AoEQuadratoMeeleAttack(hero, attackBase.GetAttack(level), animationName);
                break;
            case TypeAttack.RangeSingleTarget:
                attackDate = new SingleRangeAttack(hero, attackBase.GetAttack(level), animationName);
                break;
            case TypeAttack.RangeMultiTarget:
                attackDate = new MultyRangeAttack(hero, attackBase.GetAttack(level), animationName);
                break;
            case TypeAttack.RangeAvantiASe:
                attackDate = new SingleRangeForwardAttack(hero, attackBase.GetAttack(level), animationName);
                break;

        }

        attackDate.SetConditionAttack(attackBase);

        return attackDate;
    }

    public HeroMonster FindTarget(HeroMonster self)
    {
        if(TypeAttackActive() == null)
            return null;

        return TypeAttackActive().findHero.findHeroEnemy.FindTarget(self);
    }

    public Attack TypeAttackActive()
    {
        if (!haveAttack)
        {
            selectionAttack = null;

            if (!activePrimaryAttack)
            {
                if (attackPrimary != null)
                {
                    selectionAttack = attackPrimary;
                    haveAttack = true;
                }
                
                activePrimaryAttack = true;

                return selectionAttack;
            }

            if (hero.info.isSuper())
            {
                if (attackSuperIndex > attackSpecial.Length - 1)
                    attackSuperIndex = 0;


                if (attackSpecial[attackSuperIndex] == null)
                {
                    selectionAttack = attackBase;                   
                }
                else
                 selectionAttack = attackSpecial[attackSuperIndex];


                attackSuperIndex++;

            }
            else 
            {
                selectionAttack = attackBase;
            }

            haveAttack = true;
        }
        
        return selectionAttack;
    }

    public IEnumerator ActiveAttack()
    {
        if (hero.stateHero != BattleStateHero.None)
            yield break;
       
        TypeAttackActive();

        
        if (selectionAttack == null)
            yield break;



        yield return new WaitForSeconds(0.1f);

        if (!canAttack)
            yield break;

        hero.stateHero = BattleStateHero.Attack;

        StartCoroutine(Attack(selectionAttack.animationName,
            selectionAttack.attackInfo.GetAttackSpeed(),
            selectionAttack.attackInfo.GetAttackWaiting()));

    }

    private IEnumerator Attack(string attackName, float attackSpeed, float attackWait)
    {
        //hero.animationHero.GetAnimation().Play("AttackBase");
        hero.animationHero.TriggerAnimation(attackName);
        hero.animationHero.GetAnimation().speed = 1 / attackSpeed;
        canAttack = false;

        float delay = attackWait;
        if(hero.condition.GetConditionActive())
            delay += hero.condition.GetCondition().ActiveConditionWaitAttackboostPercent(attackWait);
        //yield return new WaitForSeconds(hero.animationHero.GetDurationTransition());
        //yield return new WaitForSeconds(hero.animationHero.GetDurationTransition());
        //yield return new WaitForSeconds(hero.animationHero.GetDurationAnimation());
        hero.stateHero = BattleStateHero.None;
        yield return new WaitForSeconds(delay);
        haveAttack = false;
        canAttack = true;

    }

    public void ViewAttackZone(TileGrid currentTile, AttackMove typeAttack, TileSelection tileSelection)
    {
        TileGrid[] tiles = null; 
        
        switch (typeAttack)
        {
            case AttackMove.Primary:
                tiles = attackPrimary?.GetAllTileTarget(currentTile);
                break;
            case AttackMove.Base:
                tiles = attackBase.GetAllTileTarget(currentTile);
                break;
            case AttackMove.Super:
                break;
        }

        

        if (tiles != null && tiles.Length > 0)
        {
            foreach (TileGrid tile in tiles)
            {
                tile.SelectTile(tileSelection);
            }
        }

        
    }
}

public enum AttackMove
{
    Primary,
    Base,
    Super
}