using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class InfoAttack
{
    [SerializeField] private TypeTarget typeTarget;
    public TypeTarget GetTypeTarget() => typeTarget;
    [SerializeField] private TypeAttack typeAttack;
    public TypeAttack GetTypeAttack() => typeAttack;
    [SerializeField] private int rangeCell = 1;
    [SerializeField] private int damage;
    public int GetDamage() => damage;
    public int GetRangeCell() => rangeCell;

    [SerializeField] private float attackSpeed = 1;
    public float GetAttackSpeed() => attackSpeed;
    [SerializeField] private float attackWaiting = 1;
    public float GetAttackWaiting() => attackWaiting;
    [SerializeField] private int rangeAttack = 1;
    public int GetRangeAttack() => rangeAttack;
}
