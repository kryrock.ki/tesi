using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack", menuName = "Custom/Attack", order = 1)]
public class AttackBase : ScriptableObject
{
    [Header("EffectObject: ")]
    [SerializeField] private GameObject effectObjectHero;
    public GameObject GetEffectObjectHero() => effectObjectHero;
    [SerializeField] private TypeEffect effectObjectHeroTile;
    public TypeEffect GetEffectObjectHerotile() => effectObjectHeroTile;
    [SerializeField] private TypeEffect effectObjectAllTileRange;
    public TypeEffect GetEffectObjectAllTile() => effectObjectAllTileRange;
    [Header("info attack on based level: ")]
    public InfoAttack[] levelAttackInfo;

    [Header("condition")]
    public ConditionAttack[] condition;

    public InfoAttack GetAttack(int level)
    {
        if (levelAttackInfo.Length - 1 < level)
            return levelAttackInfo[levelAttackInfo.Length - 1];
        return levelAttackInfo[level];
    }
}

[System.Serializable]
public enum TypeTarget
{
    First,
    last,
    Random
}

[System.Serializable]
public enum TypeAttack
{
    Single,
    TreCaselleAvantiASe,
    CaselleAttornoAQuadrato,
    CaselleAttornoARombo,
    RangeSingleTarget,
    RangeMultiTarget,
    RangePosition,
    RangeAvantiASe

}