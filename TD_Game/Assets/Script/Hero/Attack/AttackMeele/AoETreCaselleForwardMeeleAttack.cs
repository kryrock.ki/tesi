using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoETreCaselleForwardMeeleAttack : Attack
{
    public AoETreCaselleForwardMeeleAttack(HeroMonster hero, InfoAttack attack, string animationName)
    {
        heroSelf = hero;
        FindHeroType(attack.GetTypeTarget());
        attackInfo = attack;

        this.animationName = animationName;
    }

    public override void ActionAttack(HeroMonster target, int damage)
    {
        if (target)
        {

            TileGrid[] tilesTarget = GetAllTileTarget(heroSelf.tileGridCurrent);
            List<HeroMonster> enemys = new List<HeroMonster>();
            List<HeroMonster> players = new List<HeroMonster>();

            enemys.Add(target);
            players.Add(heroSelf);

            foreach (TileGrid tile in tilesTarget)
            {
                tile.GetConditionParticle().ActiveParticle(typeEffectAllTile);

                if (tile.GetOccupated() && tile.GetHero() != heroSelf)
                {
                    if (tile.GetHero().GetTeam().GetTeamId() != heroSelf.GetTeam().GetTeamId())
                    {
                        if (tile.GetHero() == target)
                        {
                            target.Damage(damage, heroSelf);
                        }

                        if (!enemys.Contains(tile.GetHero()))
                        {
                            tile.GetHero()?.Damage(damage, heroSelf);
                            enemys.Add(tile.GetHero());
                        }
                    }
                    else if (!players.Contains(tile.GetHero()))
                    {
                        players.Add(tile.GetHero());
                    }
                }
            }

            heroSelf.tileGridCurrent.GetConditionParticle().ActiveParticle(typeEffectTileHero);

            if (objectPool != null)
            {
                GameObject bullet = objectPool.Get();
                bullet.GetComponent<RangeBullet>().Setup(heroSelf, 0, attackInfo, target.gameObject);
            }

            SetCondition(enemys.ToArray(), players.ToArray());
        }
        
    }

    public TileGrid[] AllTileTarget(TileGrid tileCurrent, Vector3 forward)
    {
        List<TileGrid> tilesTarget = new List<TileGrid>();

        TileGrid startTile = tileCurrent;

        int range = 0;
        while (range < attackInfo.GetRangeAttack())
        {
            Vector3 tilePos = startTile.transform.localPosition + (forward * GridManager.instance.GetSpaceTile());
            tilePos = new Vector3((int)tilePos.x, (int)tilePos.y, (int)tilePos.z);
            TileGrid tile = GridManager.instance.GetTileBasedonPosition(tilePos);

            if (tile == null)
                break;

            tilesTarget.Add(tile);

            foreach (TileGrid tl in tile.GetNeighborn())
            {
                if (tl.IsConteinerNeighbornTile(startTile))
                    tilesTarget.Add(tl);
            }
            foreach (TileGrid tl in tile.GetNeighbornDiagonal())
            {
                if (tl.IsConteinerNeighbornTile(startTile))
                    tilesTarget.Add(tl);
            }

            startTile = tile;

            range++;
        }

        return tilesTarget.ToArray();
    }

    public override TileGrid[] GetAllTileTarget(TileGrid tileGrid)
    {
        Vector3 forward = new Vector3(Mathf.Sign(heroSelf.transform.forward.x), 0, Mathf.Sign(heroSelf.transform.forward.x));
        return AllTileTarget(tileGrid, forward);
    }
}
