using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attack
{
    public HeroMonster heroSelf;
    public FindHeroScript findHero;
    public ObjectPool objectPool;
    public InfoAttack attackInfo;
    public string animationName;
    public TypeEffect typeEffectTileHero;
    public TypeEffect typeEffectAllTile;
    public GameObject typeEffectHero;
    public ConditionAttack[] condition;
    public void FindHeroType(TypeTarget typeTarget)
    {
        findHero = new FindHeroScript(typeTarget);
    }

    public void SetConditionAttack(AttackBase attack)
    {
        typeEffectTileHero = attack.GetEffectObjectHerotile();
        typeEffectAllTile = attack.GetEffectObjectAllTile();
        this.typeEffectHero = attack.GetEffectObjectHero();
        this.condition = attack.condition;

        if (typeEffectHero)
            objectPool = new ObjectPool(1, typeEffectHero, Vector3.zero, heroSelf.GetTransformAttack());
    }


    public abstract void ActionAttack(HeroMonster target, int damage);

    public abstract TileGrid[] GetAllTileTarget(TileGrid tileGrid);

    public void SetCondition(HeroMonster[] targetsEnemy = null, HeroMonster[] targetsPlayer = null)
    {

        foreach (ConditionAttack condition in condition)
        {
            if (condition.conditionObject == null || condition.conditionObject.conditionSetup.GetTypeCondition() == TypeCondition.None)
                return;

            switch (condition.GetTypeTargetCondition())
            {
                case TypeTargetCondition.AllTargetEnemy:
                    foreach (HeroMonster hero in targetsEnemy)
                    {
                        if(!hero.death.GetDeath())
                        hero.condition.AddCondition(condition.conditionObject);
                    }
                    break;
                case TypeTargetCondition.AllEnemy:
                    foreach (HeroMonster hero in GameObject.FindObjectsOfType<HeroMonster>())
                    {
                        if (hero.GetTeam().GetTeamId() != targetsPlayer[0].GetTeam().GetTeamId() && !hero.death.GetDeath())
                        {
                            hero.condition.AddCondition(condition.conditionObject);
                        }
                    }
                    break;
                case TypeTargetCondition.TargetEnemy:
                    targetsEnemy[0].condition.AddCondition(condition.conditionObject);
                    break;
                case TypeTargetCondition.AllPlayer:
                    foreach (HeroMonster hero in GameObject.FindObjectsOfType<HeroMonster>())
                    {
                        if (hero.GetTeam().GetTeamId() == targetsPlayer[0].GetTeam().GetTeamId() && !hero.death.GetDeath())
                        {
                            hero.condition.AddCondition(condition.conditionObject);
                        }
                    }
                    break;
                case TypeTargetCondition.AllTargetPlayer:
                    foreach (HeroMonster hero in targetsPlayer)
                    {
                        if (!hero.death.GetDeath())
                            hero.condition.AddCondition(condition.conditionObject);
                    }
                    break;
                case TypeTargetCondition.SelfPlayer:
                    targetsPlayer[0].condition.AddCondition(condition.conditionObject);
                    break;

            }
        }


        
    }
}
