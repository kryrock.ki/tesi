using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoEQuadratoMeeleAttack : Attack
{
    public AoEQuadratoMeeleAttack(HeroMonster hero, InfoAttack attack, string animationName)
    {
        heroSelf = hero;
        FindHeroType(attack.GetTypeTarget());
        attackInfo = attack;

        this.animationName = animationName;
    }

    public override void ActionAttack(HeroMonster target, int damage)
    {
        TileGrid[] tileneighborn = GetAllTileTarget(heroSelf.tileGridCurrent);
        List<HeroMonster> enemys = new List<HeroMonster>();
        List<HeroMonster> players = new List<HeroMonster>();

        enemys.Add(target);
        players.Add(heroSelf);


        foreach (TileGrid tile in tileneighborn)
        {
            tile.GetConditionParticle().ActiveParticle(typeEffectAllTile);

            if (!tile.GetOccupated())
                continue;

            if (tile.GetHero() != heroSelf && tile.GetHero() != null)
            {
                if (tile.GetHero().GetTeam().GetTeamId() != heroSelf.GetTeam().GetTeamId())
                {
                    if (tile.GetHero() == target)
                    {
                        target.Damage(damage, heroSelf);
                    }

                    if (!enemys.Contains(tile.GetHero()))
                    {
                        tile.GetHero()?.Damage(damage, heroSelf);
                        enemys.Add(tile.GetHero());

                    }
                }
                else if (!players.Contains(tile.GetHero()))
                {
                    players.Add(tile.GetHero());
                }
            }
        }

        heroSelf.tileGridCurrent.GetConditionParticle().ActiveParticle(typeEffectTileHero);


        //heroSelf.SpawnEffect(attackBase.GetEffectObjectHero(), heroSelf.transform);
        if (objectPool != null)
        {
            GameObject bullet = objectPool.Get();
            bullet.GetComponent<RangeBullet>().Setup(heroSelf, 0, attackInfo, target.gameObject);
        }

        SetCondition(enemys.ToArray(), players.ToArray());
    }

    public TileGrid[] AllTileTarget(TileGrid tileCurrent, int rangeMax)
    {
        List<TileGrid> tileneighborn = new List<TileGrid>();
        List<TileGrid> control = new List<TileGrid>();

        tileneighborn.AddRange(tileCurrent.GetNeighborn());
        tileneighborn.AddRange(tileCurrent.GetNeighbornDiagonal());

        int range = 1;

        while (range < rangeMax)
        {
            foreach (TileGrid tile in tileneighborn)
            {
                control.AddRange(tile.GetNeighborn());
                control.AddRange(tile.GetNeighbornDiagonal());
            }

            foreach (TileGrid tile in control)
            {
                
                
                if (!tileneighborn.Contains(tile) && tile != tileCurrent)
                    tileneighborn.Add(tile);
                
            }

            control.Clear();
            range++;
        }

        return tileneighborn.ToArray();
    }

    public override TileGrid[] GetAllTileTarget(TileGrid tileGrid)
    {
        return AllTileTarget(tileGrid, attackInfo.GetRangeAttack());
    }
}
