using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleMeeleAttack : Attack
{
    public SingleMeeleAttack(HeroMonster hero, InfoAttack attack, string animationName)
    {
        heroSelf = hero;
        FindHeroType(attack.GetTypeTarget());
        attackInfo = attack;

        this.animationName = animationName;
    }

    public override void ActionAttack(HeroMonster target, int damage)
    {
        if (target)
        {
            List<HeroMonster> enemys = new List<HeroMonster>();
            List<HeroMonster> players = new List<HeroMonster>();

            enemys.Add(target);
            players.Add(heroSelf);

            heroSelf.tileGridCurrent.GetConditionParticle().ActiveParticle(typeEffectTileHero);

            if (objectPool != null)
            {
                GameObject bullet = objectPool.Get();
                bullet.GetComponent<RangeBullet>().Setup(heroSelf, 0, attackInfo, target.gameObject);
            }

            target.Damage(damage, heroSelf);

            SetCondition(enemys.ToArray(), players.ToArray());
        }
        
    }

    public override TileGrid[] GetAllTileTarget(TileGrid tileGrid)
    {
        return null;
    }
}
