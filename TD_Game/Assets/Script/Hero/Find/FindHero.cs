using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FindHero
{
    public HeroMonster target;
    public HeroMonster self;
    public abstract HeroMonster FindTarget(HeroMonster self, List<HeroMonster> herosNoTarget = null);

}
