using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindRandomTarget : FindHero
{
    public override HeroMonster FindTarget(HeroMonster self, List<HeroMonster> herosNoTarget = null)
    {
        HeroMonster target = null;

        HeroMonster[] enemis = GameManagerBattle.instance.GetAllTypeHeroInGame(self.GetTeam().GetTeamId()).ToArray();

        bool targetCorrect = false;

        while (!targetCorrect)
        {
            target = enemis[Random.Range(0, enemis.Length - 1)];

            

            if (herosNoTarget != null)
            {
                if (herosNoTarget.Contains(target))
                {
                    targetCorrect = false;
                }
                else
                {
                    targetCorrect = true;
                }
            }
            else
            {
                targetCorrect = true;
            }
        }
        

        return target;
    }

    
}
