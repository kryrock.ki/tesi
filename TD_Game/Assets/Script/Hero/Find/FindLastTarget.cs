using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindLastTarget : FindHero
{
    public override HeroMonster FindTarget(HeroMonster self, List<HeroMonster> herosNoTarget = null)
    {
        HeroMonster target = null;
        float distance = 0;

        foreach (HeroMonster hero in GameManagerBattle.instance.GetAllTypeHeroInGame(self.GetTeam().GetTeamId()))
        {
            float distanceLocation = Vector3.Distance(self.transform.position, hero.transform.position);

            if (hero.death.GetDeath())
                continue;

            if (distanceLocation > distance)
            {
                if (herosNoTarget != null)
                {
                    if (!herosNoTarget.Contains(hero))
                    {
                        target = hero;
                        distance = distanceLocation;

                    }

                }
                else
                {
                    target = hero;
                    distance = distanceLocation;
                }              
            }
            else if (distance == distanceLocation)
            {
                foreach (TileGrid tile in self.tileGridCurrent.GetNeighborn())
                {
                    if(tile.GetOccupated() && tile.GetHero() == hero)
                    {
                        target = hero;
                        distance = distanceLocation;
                    }
                }
            }
        }

        return target;
    }

    
}
