using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindFirstTarget : FindHero
{
    public override HeroMonster FindTarget(HeroMonster self, List<HeroMonster> herosNoTarget = null)
    {       
        HeroMonster target = null;
        float distance = Mathf.Infinity;

        foreach (HeroMonster hero in GameManagerBattle.instance.GetAllTypeHeroInGame(self.GetTeam().GetTeamId()))
        {
            float distanceLocation = Vector3.Distance(self.transform.position, hero.transform.position);

            if (distanceLocation < distance && !hero.death.GetDeath())
            {
                if (SetTarget(hero, herosNoTarget))
                {
                    target = hero;
                    distance = distanceLocation;
                }
            }
            else if (distance == distanceLocation && !hero.death.GetDeath())
            {
                foreach (TileGrid tile in self.tileGridCurrent.GetNeighborn())
                {
                    if (SetTarget(hero, herosNoTarget))
                    {
                        target = hero;
                        distance = distanceLocation;
                    }
                }
            }

        }

        return target;
    }

    private bool SetTarget(HeroMonster heroTarget, List<HeroMonster> NotTarget)
    {

        if (NotTarget != null)
        {
            if (!NotTarget.Contains(heroTarget))
            {
                return true;
            }
        }
        else
        {
            return true;
        }

        return false;
    }
}
