using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindHeroScript
{
    public FindHero findHeroEnemy;

    public FindHeroScript(TypeTarget typeTarget)
    {
        SetFindHero(typeTarget);
    }
    private void SetFindHero(TypeTarget typeTarget)
    {
        switch (typeTarget)
        {
            case TypeTarget.First:
                findHeroEnemy = new FindFirstTarget();
                break;
            case TypeTarget.last:
                findHeroEnemy = new FindLastTarget();
                break;
            case TypeTarget.Random:
                findHeroEnemy = new FindRandomTarget();
                break;
        }
    }
}
