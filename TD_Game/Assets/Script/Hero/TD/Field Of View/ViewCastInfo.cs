using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ViewCastInfo
{
    public bool hit;
    public Vector3 point;
    public float dst;
    public float angle;

    public ViewCastInfo(bool hit, Vector3 point, float dst, float angle)
    {
        this.hit = hit;
        this.point = point;
        this.dst = dst;
        this.angle = angle;
    }
}
