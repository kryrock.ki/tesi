using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour
{
    [SerializeField] private GameObject hero;
    [SerializeField] private Material defaultMaterial;
    [SerializeField] private Material RedMaterial;

    public bool place;
    // Start is called before the first frame update
    void Start()
    {
        place = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 7 || other.gameObject.layer == 8)
        {
            hero.GetComponent<MeshRenderer>().material = RedMaterial;
            place = false;
        }


    }

    private void OnTriggerExit(Collider other)
    {
        hero.GetComponent<MeshRenderer>().material = defaultMaterial;
        place = true;

    }
}
