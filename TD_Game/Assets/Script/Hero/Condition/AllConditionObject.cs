using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "AllCondition", menuName = "Custom/Condition/All", order = 2)]
public class AllConditionObject : ScriptableObject
{
    public ConditionObject[] conditions;
}
