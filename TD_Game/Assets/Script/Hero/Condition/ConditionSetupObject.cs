using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class ConditionSetupObject
{
    [SerializeField] private TypeCondition typeCondition;
    public TypeCondition GetTypeCondition() => typeCondition;

    [SerializeField] private Sprite image;
    public Sprite GetImage() => image;
}
