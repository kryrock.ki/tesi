using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonCondition : ConditionEffect
{
    public bool attack;

    public override float ActiveConditionWaitAttackboostPercent(float value)
    {
        switch (condition.condition.GetBoostCondition())
        {
            case Boost.BuffSpeedAttack:
                return -((value * condition.condition.GetPercentBoost()) / 100);
            case Boost.DebuffSpeedAttack:
                return (value * condition.condition.GetPercentBoost()) / 100;
            default:
                return 0;
        }     
    }

    public override IEnumerator ActiveConditionOnSecond()
    {
        if (attack)
            yield break;

        attack = true;
        yield return new WaitForSeconds(condition.condition.GetSecondDamage());
        hero.Damage(condition.condition.GetDamageCondition(), null);
        attack = false;
    }

    public override float ActiveConditionProtectiveBootPercent(float value)
    {
        return 0;
    }

    public override float ActiveConditionDamageBoostPercent(float value)
    {
        switch (condition.condition.GetBoostCondition())
        {
            case Boost.BuffDamage:
                return ((value * condition.condition.GetPercentBoost()) / 100);
            case Boost.DebuffDamage:
                return -((value * condition.condition.GetPercentBoost()) / 100);
            default:
                return 0;
        }
    }
}
