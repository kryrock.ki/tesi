using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ConditionEffect
{
    public HeroMonster hero;
    public ConditionObject condition;

    public void Setup(HeroMonster hero, ConditionObject condition)
    {
        this.hero = hero;
        this.condition = condition;
    }
    public abstract IEnumerator ActiveConditionOnSecond();
    public abstract float ActiveConditionProtectiveBootPercent(float value);
    public abstract float ActiveConditionWaitAttackboostPercent(float value);
    public abstract float ActiveConditionDamageBoostPercent(float value);
}
