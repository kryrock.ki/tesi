using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class ConditionParticle
{
    public ConditionParticleEffect[] conditionParticles;

    public void ActiveParticle(TypeEffect typeEffect)
    {
        foreach (ConditionParticleEffect cp in conditionParticles)
        {
            cp.ActiveTypeEffect(typeEffect);
        }
    }
}
