using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IConditionParticle
{
    public void ActiveTypeEffect(TypeEffect typeEffect);
    public void ActiveTypeCondition(TypeCondition typeEffect);
}
