using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Condition
{
    [SerializeField] private int damageCondition;
    public int GetDamageCondition() => damageCondition;
    
    [SerializeField] private float secondDamage;
    public float GetSecondDamage() => secondDamage;

    [SerializeField] private float durationCondition;
    public float GetDurationCondition() => durationCondition;

    [Header("if effect have second effect")]
    [SerializeField] private Boost boostCondition;
    public Boost GetBoostCondition() => boostCondition;

    [SerializeField, Range(0, 100)] private int percentBoost;
    public int GetPercentBoost() => percentBoost;
}

[System.Serializable]
public enum TypeCondition
{
    None,
    Poison,
    Fire,
    Shield,
    BuffSpeedAttack,
    DebuffSpeedAttack,
    BuffDamage,
    DebuffDamage
}

[System.Serializable]
public enum Boost
{
    None,
    BuffSpeedAttack,
    DebuffSpeedAttack,
    BuffDamage,
    DebuffDamage
}

[System.Serializable]
public enum TypeTargetCondition
{
    AllEnemy,
    AllTargetEnemy,
    TargetEnemy,
    AllPlayer,
    AllTargetPlayer,
    SelfPlayer,
}