using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class ConditionAttack
{
    public ConditionObject conditionObject;

    [SerializeField] private TypeTargetCondition typeTargetCondition;
    public TypeTargetCondition GetTypeTargetCondition() => typeTargetCondition;
}
