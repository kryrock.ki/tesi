using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConditionHero : MonoBehaviour
{
    public ConditionParticleType[] conditionParticle;

    private bool isCondition;
    public bool GetConditionActive() => isCondition;
    public ConditionObject condition;

    private Dictionary<TypeCondition, ConditionEffect> conditionList;
    public Dictionary<TypeCondition, ConditionEffect> GetConditionEffect() => conditionList;

    private ConditionEffect conditionEffect;
    public ConditionEffect GetCondition() => conditionEffect;

    private HeroMonster hero;
    public float durationCount;

    #region Event

    public UnityAction<TypeCondition> StartconditionEvent;
    public void StartConditionInvok(TypeCondition typeCondition) => StartconditionEvent.Invoke(typeCondition);
    public UnityAction conditionEvent;
    public void ConditionInvok() => conditionEvent.Invoke();
    public UnityAction EndconditionEvent;
    public void EndConditionInvok() => EndconditionEvent.Invoke();

    #endregion



    private void Start()
    {
        Setup();
    }

    public void Setup()
    {
        this.hero = GetComponentInParent<HeroMonster>();
        hero.condition.EndconditionEvent += RemoveAllCondition;
        conditionList = new Dictionary<TypeCondition, ConditionEffect>();
        conditionList.Add(TypeCondition.Poison, new PoisonCondition());
        conditionList.Add(TypeCondition.Fire, new FireCondition());
        conditionList.Add(TypeCondition.Shield, new ShieldCondition());
        conditionList.Add(TypeCondition.BuffDamage, new PoisonCondition());
    }

    private void SetParticle(TypeCondition typeCondition)
    {
        foreach (ConditionParticleType cp in conditionParticle)
        {
            cp.ActiveTypeCondition(typeCondition);
        }
    }

    public void AddCondition(ConditionObject condition)
    {
        this.condition = condition;
        durationCount = 100;

        SetParticle(condition.conditionSetup.GetTypeCondition());

        conditionList.TryGetValue(condition.conditionSetup.GetTypeCondition(), out conditionEffect);
        conditionEffect.Setup(hero, condition);

        hero.condition.StartConditionInvok(condition.conditionSetup.GetTypeCondition());
        isCondition = true;
    }

    public void RemoveCondition()
    {
        if (!condition)
            return;

        durationCount -= 100 * (Time.deltaTime / condition.condition.GetDurationCondition());
        
        
        if (durationCount < 1)
        {
            hero.condition.EndConditionInvok();
        }
        else
        {
            hero.StartCoroutine(conditionEffect.ActiveConditionOnSecond());
            hero.condition.ConditionInvok();
        }
    }

    private void RemoveAllCondition()
    {
        isCondition = false;
        SetParticle(TypeCondition.None);
    }
}
