using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ConditionParticleType : IConditionParticle
{
    public TypeCondition typeEffect;
    public ParticleSystem effectobject;

    public void ActiveTypeCondition(TypeCondition typeEffect)
    {
        if (this.typeEffect == typeEffect)
        {
            effectobject.gameObject.SetActive(true);
            effectobject.Play();
        }
        else
        {
            effectobject.Clear();
            effectobject.gameObject.SetActive(false);
        }
    }

    public void ActiveTypeEffect(TypeEffect typeEffect)
    {
        return;
    }
}
