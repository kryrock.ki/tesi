using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "condition", menuName = "Custom/Condition/Single", order = 2)]
public class ConditionObject : ScriptableObject
{
    [Header("Setup: ")]
    public ConditionSetupObject conditionSetup;
    [Header("SetupAttack: ")]
    public Condition condition;


}
