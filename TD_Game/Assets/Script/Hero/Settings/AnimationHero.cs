using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationHero
{
    private Animator animationControl;
    public Animator GetAnimation() => animationControl;
    public AnimationHero(Animator ac)
    {
        animationControl = ac;
    }

    public void TriggerAnimation(string triggerText)
    {
        animationControl.SetTrigger(triggerText);
    }

    public void ResetTriggerAnimation(string triggerText)
    {
        animationControl.ResetTrigger(triggerText);
    }

    public void BoolAnimation(string triggerText, bool value)
    {
        animationControl.SetBool(triggerText, value);
    }

    public float GetDurationAnimation()
    {
        AnimatorClipInfo[] m_CurrentClipInfo = animationControl.GetCurrentAnimatorClipInfo(0);

        if (m_CurrentClipInfo.Length > 0)
        {
            return m_CurrentClipInfo[0].clip.length;
        }

        return 0;
    }

    public float GetDurationTransition()
    {
        return animationControl.GetAnimatorTransitionInfo(0).duration + 0.1f;
    }

    public IEnumerator ScaleAnimation(GameObject gameObject)
    {
        gameObject.transform.DOScale(Vector3.one * 1.1f, 0.3f);
        yield return new WaitForSeconds(0.3f);
        gameObject.transform.DOScale(Vector3.one, 0.3f);
        yield return new WaitForSeconds(0.3f);
        gameObject.transform.localScale = Vector3.one;

    }

}
