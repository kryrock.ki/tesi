using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiHero : MonoBehaviour
{
    [SerializeField] private HeroMonster hero;
    private Vector3 rotationHero;
    [SerializeField] private GameObject healtBar;
    [SerializeField] private GameObject superBar;
    [SerializeField] private GameObject condition;
    [SerializeField] private AllConditionObject ConditionObject;
    [SerializeField] private Image healtImage;
    // Start is called before the first frame update
    void Start()
    {
        rotationHero = hero.transform.eulerAngles;
        SetRotationBar();
        SetCondition(false, TypeCondition.None);
        Setup();
        GameManagerBattle.instance.startBattle.AddListener(Setup);
        //GameManagerBattle.instance.startBattle.AddListener(() => SetActive(true, true));
        hero.damageEvent += Setup;
        hero.info.superEvent += Setup;
        hero.death.deathEvent.AddListener(() => SetActive());
        hero.condition.StartconditionEvent += (typeCondition) => SetCondition(true, typeCondition);
        hero.condition.conditionEvent += SetConditionValue;
        hero.condition.EndconditionEvent += () => SetCondition(false, TypeCondition.None);

        healtImage.color = hero.GetTeam().GetColor();

        if(hero.GetTeam().GetTeamId() % 2 == 0)
            transform.parent.transform.localPosition = new Vector3(0, 5, 0);
        else
            transform.parent.transform.localPosition = new Vector3(0, 9, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (rotationHero.y != hero.transform.eulerAngles.y)
        {
            SetRotationBar();
        }
        
    }
    private void SetRotationBar()
    {
        transform.eulerAngles = new Vector3(Camera.main.transform.eulerAngles.x, hero.transform.eulerAngles.y - hero.transform.eulerAngles.y, 0);
        rotationHero = hero.transform.eulerAngles;
    }

    public void SetActive(bool healtBar = false, bool superBar = false, bool condition = false)
    {
        this.healtBar.SetActive(healtBar);
        if(hero?.info.GetAttackSpecialInfo().Length > 0)
            this.superBar.SetActive(superBar);
        else
            this.superBar.SetActive(false);
        this.condition.SetActive(condition);
    }

    private void SetCondition(bool active, TypeCondition typeCondition)
    {
        condition.SetActive(active);
        condition.GetComponentInChildren<Slider>().maxValue = 100;
        condition.GetComponentInChildren<Slider>().value = hero.condition.durationCount;
        
        foreach (ConditionObject cdt in ConditionObject.conditions)
        {
            if (cdt.conditionSetup.GetTypeCondition() == typeCondition)
            {
                condition.GetComponentInChildren<Image>().sprite = cdt.conditionSetup.GetImage();
            }
        }
    }
    private void SetConditionValue()
    {
        condition.GetComponentInChildren<Slider>().value = hero.condition.durationCount;
    }

    public void Setup()
    {
        healtBar.GetComponentInChildren<Slider>().maxValue = hero.info.GetHealtMax();
        healtBar.GetComponentInChildren<Slider>().value = hero.info.GetHealtCurrent();

        superBar.GetComponentInChildren<Slider>().maxValue = hero.info.GetInfoHero().GetMaxSuper();
        superBar.GetComponentInChildren<Slider>().value = hero.info.GetSuperCurrent();
    }
}
