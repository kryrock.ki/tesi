using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeroScript : HeroMonster
{
    private void Start()
    {       
        GameManagerBattle.instance.startBattle.AddListener(() => { if(gameObject.activeSelf) StartCoroutine(SetupBattle()); });
        GameManagerBattle.instance.endTurn.AddListener((TypeHero) => EndTurn());
        GameManagerBattle.instance.troopPreparetionGrid.AddListener(SetPreparetion);
        move.endMoving += SetTarget;
    }

    private IEnumerator BattleUpdate()
    { 
   
        while (mode != Mode.None && !death.GetDeath())
        {
            yield return new WaitForEndOfFrame();

            if (!IsTarget())
            {
                SetTarget();
            }

            if (IsInRange() && stateHero != BattleStateHero.Move)
            {               

                Quaternion rotation = Quaternion.LookRotation(target.transform.position - transform.position);

                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 20);

                if (attack.canAttack)
                {
                    StartCoroutine(attack.ActiveAttack());
                }
                
            }
            else if(stateHero != BattleStateHero.Attack)
            {

                move.Moving(target, tileGridCurrent);
            }

            //info.SuperActive();


            if(condition.GetConditionActive())
            {
                condition.RemoveCondition();
                
            }

            yield return null;

        }
    }

    private IEnumerator SetupBattle()
    {
        stateHero = BattleStateHero.None;
        GetComponent<SelectionHero>().SelectionAction(false, false, true);
        death.Reset();
        target = null;

        switch (mode)
        {
            case Mode.Selection:
            case Mode.InGame:
                SetMode(Mode.InGame);
                attack.SetupBattle();
                move.SetupBattle();
                SetMeshRender(true, false);
                ui.SetActive(true, true);
                yield return new WaitForSeconds(1f);
                
                StartCoroutine(attack.ActiveAttack());
                yield return new WaitForSeconds(2f);
                StartCoroutine("BattleUpdate");
                break;
            default:
                yield return new WaitForSeconds(1f);
                ui.SetActive();
                gameObject.SetActive(false);
                break;
        }
    }
    private void SetTarget()
    {
        HeroMonster heroTarget = attack.FindTarget(this);
 
        if (heroTarget == null)
            return;
        
        target = heroTarget;

        Quaternion rotation = Quaternion.LookRotation(target.gameObject.transform.position - transform.position);
        
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 20);
    }

    private void EndTurn()
    {
        
        StopCoroutine("BattleUpdate");
        target = null;

        condition.EndConditionInvok();

        animationHero.GetAnimation().speed = 1;
        animationHero.BoolAnimation("Run", false);
        animationHero.BoolAnimation("Empty", false);
    }

  
    
}
