using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hero", menuName ="Custom/Hero", order = 0)]
public class HeroObject : ScriptableObject
{
    [Header("info: ")]
    [SerializeField] private string nameHero;
    public string GetName() => nameHero;
    [SerializeField] private Rarity rarity;
    public Rarity GetRarity() => rarity;
    [Range(1, 10)]
    public int level;
    public int GetLevel() => level;
    private int levelMax = 10;

    [Header("Setting: ")]
    public InfoHero info;
}

[System.Serializable]

public enum Rarity
{
    Common,
    Rare,
    Epic,
    Legendary,
    Hero
}