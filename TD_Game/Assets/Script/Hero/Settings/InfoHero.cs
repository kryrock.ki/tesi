using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InfoHero
{
    [SerializeField] private int cost;
    [SerializeField] public int GetCost() => cost;
    [SerializeField] private TypeHeroAttack typeHero; 
    public TypeHeroAttack GetTypeHeroAttack() => typeHero; 
    [SerializeField] private int healt;
    public int GetHealtMax() => healt;
    [SerializeField, Range(1,10)] private float superIncrement;
    public float GetIncrementSuper() => superIncrement;
    [SerializeField, Range(0, 10)] private float superDamage;
    public float GetDamageIncrementSuper() => superDamage;
    [SerializeField, Range(1,100)] private float superMax;
    public float GetMaxSuper() => superMax;
    [SerializeField] private float movingSpeedBase;
    public float GetMovingSpeed() => movingSpeedBase;

    [Header("Attacked: ")]
    public AttackBase AttackPrimary;
    public AttackBase AttackBase;
    public AttackBase[] AttackSpecial;
}

[System.Serializable]
public enum TypeHeroAttack
{
    Tank,
    Archer,
    Wizard,
    SupportMeele,
    SupportRange,
    murderer
}