using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System.Linq;

[RequireComponent(typeof(SelectionHero))]
public class HeroMonster : MonoBehaviour
{
    public TypeHeroControl TypeControl;
    public Mode mode;
    public BattleStateHero stateHero;
    public void SetBattleState(BattleStateHero battleState) => stateHero = battleState;
    public AnimationHero animationHero;

    public bool isGame;
    public TileGrid tileGridCurrent;
    [SerializeField] protected TileGrid tileGridStartPosition;
    public InfoHeroScript info;
    public HeroMonster target;
    public AttackScript attack;
    public ConditionHero condition;
    public Moved move;
    public SelectionHero selection;
    public Death death;
    public UiHero ui;

    private GameObject shopPos;
    private Collider collisionComponent;
    [Header("Object Ref: ")]
    [SerializeField] private GameObject DefaultHero;
    [SerializeField] private GameObject NotSelectionHero;
    [SerializeField] private GameObject GridSelectedHero;
    [SerializeField] private ParticleSystem particleChangePose;
    [SerializeField] private ParticleSystem particleHit;
    [SerializeField] private Transform attackTransform;
    public Transform GetTransformAttack() => attackTransform;

    public Squad refSquad;
    public Squad GetTeam() => refSquad;
    #region Events

    public UnityAction damageEvent;
    public void DamageInvok() => damageEvent.Invoke();

    #endregion

    private void Awake()
    {
        info.Setup();

        animationHero = new AnimationHero(GetComponentInChildren<Animator>());

        attack = GetComponentInChildren<AttackScript>();
        collisionComponent = GetComponentInChildren<Collider>();

        GridSelectedHero.transform.localScale = new Vector3(GridManager.instance.GetGridData().GetTileSize().x, 1, GridManager.instance.GetGridData().GetTileSize().y);
        SetMeshRender(true, false);

        ActivetedParticle(false);
        particleHit.gameObject.SetActive(false);

        death.Setup(this);
        move.Setup(this);
        selection = GetComponent<SelectionHero>();
        condition = GetComponentInChildren<ConditionHero>();
        ui = GetComponentInChildren<UiHero>();
        GameObject.FindObjectOfType<PlayerAction>().DragHeroEvent += (typeHero, hero) => DragInGame(hero);
        GameObject.FindObjectOfType<PlayerAction>().DropHeroEvent += (isPlace, tile, hero) => DropInGame(isPlace, tile, hero);
        
    }

    public void Setup(TypeHeroControl typeHero, Squad squad, GameObject shop = null)
    {
        TypeControl = typeHero;
        shopPos = shop;
        refSquad = squad;
        isGame = false;
        //SetCPUHero();
        //SetPlayerHero();
    }

    public void Damage(int damage, HeroMonster RefHeroAttack)
    {
        
        RefHeroAttack?.info.SuperActive();
        
        info.SuperActive(true);

        if (condition.GetConditionActive())
        {                
            damage += (int)condition.GetCondition().ActiveConditionProtectiveBootPercent(damage);
        }

        if (info.SetHealtCurrent(damage) < 1)
        {
            death.SetDead(true);
            GameManagerBattle.instance.EndGame(refSquad.GetTeamId());
        }

        if(damageEvent.GetInvocationList() != null || damageEvent.GetInvocationList().Length != 0)
            DamageInvok();

        particleHit.gameObject.SetActive(true);
        StartCoroutine(animationHero.ScaleAnimation(gameObject));
    }

    public bool IsInRange() 
    {
        if (target != null)
        {
            List<TileGrid> tiles = TilesRange(tileGridCurrent, attack.TypeAttackActive()).ToList();
            
            if(tiles.Count < 1)
                return false;

            foreach (TileGrid tile in tiles)
            {
                if (tile.GetHero() == target)
                {
                    return true;
                }
            }

                //List<TileGrid> tileneighborn = new List<TileGrid>();
                //List<TileGrid> control = new List<TileGrid>();
                //
                //tileneighborn.AddRange(tileGridCurrent.GetNeighborn());
                //tileneighborn.AddRange(tileGridCurrent.GetNeighbornDiagonal());
                //
                //int range = 0;
                //
                //if (attack.TypeAttackActive() == null)
                //{
                //    Debug.Log("Not Special Attack");
                //    return false;
                //}
                //
                //while (range < attack.TypeAttackActive().attackInfo.GetRangeCell())
                //{               
                //    foreach (TileGrid tile in tileneighborn)
                //    {
                //        if (tile.GetHero() == target)
                //        {
                //            return true;
                //        }
                //
                //        control.AddRange(tile.GetNeighborn());
                //        control.AddRange(tile.GetNeighbornDiagonal());
                //    }
                //
                //    foreach (TileGrid tile in control)
                //    {
                //        if (!tileneighborn.Contains(tile))
                //            tileneighborn.Add(tile);
                //    }
                //
                //    control.Clear();
                //    range++;
                //}



        }

        return false;
    }

    public TileGrid[] TilesRange(TileGrid tileCurrent, Attack typeAttack)
    {
        List<TileGrid> tileneighborn = new List<TileGrid>();
        List<TileGrid> control = new List<TileGrid>();

        tileneighborn.AddRange(tileCurrent.GetNeighborn());
        tileneighborn.AddRange(tileCurrent.GetNeighbornDiagonal());

        int range = 0;

        if (typeAttack == null)
            return null;

        while (range < typeAttack.attackInfo.GetRangeCell() - 1)
        {
            foreach (TileGrid tile in tileneighborn)
            {

                control.AddRange(tile.GetNeighborn());
                control.AddRange(tile.GetNeighbornDiagonal());
            }

            foreach (TileGrid tile in control)
            {
                if (!tileneighborn.Contains(tile))
                    tileneighborn.Add(tile);
            }

            control.Clear();
            range++;
        }

        tileneighborn.Remove(tileCurrent);

        return tileneighborn.ToArray();
    }

    public bool IsTarget()
    {
        if (target == null)
            return false;
        if (target.death.GetDeath())
            return false;

        return true;
    }
    
  
    public void SetMeshRender(bool defaultMesh, bool notSel)
    {
        DefaultHero.SetActive(defaultMesh);
        NotSelectionHero.SetActive(notSel);
        GridSelectedHero.SetActive(mode == Mode.Selection && TypeControl == TypeHeroControl.Player);
    }
    public void SetMode(Mode val) => mode = val;
    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }
    public void SetTile(TileGrid tile) => tileGridCurrent = tile;
    public bool ChangeTile(TileGrid tile)
    {
        if (tile != null && tile != tileGridCurrent)
        {            
            if (tile.GetOccupated())
            { 
                if (tile.GetHero().mode == Mode.InGame)
                {

                    if(tileGridStartPosition != null)
                        DropInGame(true, tileGridStartPosition, this);
                    else
                        ReturnToShop();


                    return false;
                }


                tile.GetHero().SetTileCurrent(tileGridCurrent, true);

                SetTileCurrent(tile, true);
            }
            else
            {
                SetTileCurrent(tile, false);
            }                   
        }
        else
        {
            SetTileCurrent(tileGridCurrent, false);
        }

        return true;
     
    }
    public void DropInGame(bool isPlace, TileGrid tile, HeroMonster heroSel)
    {
        if(collisionComponent != null && !collisionComponent.enabled)
            collisionComponent.enabled = true;

        bool changeTile = false;

        if (heroSel == this)
        {
        
            if (isPlace)
            {
                if (changeTile = ChangeTile(tile))
                {

                    if (!isGame)
                    {
                        if (heroSel.info.GetRarity() != Rarity.Hero)
                        {
                            refSquad.RemoveElisr(info.GetInfoHero().GetCost());
                            
                            //GetComponent<SelectionHero>().SelectionAction()
                        }
                        
                        if (heroSel.TypeControl == TypeHeroControl.Player)
                        {
                            shopPos?.GetComponent<TargetShop>().SetVisibleInfo(false);
                        }

                        refSquad.InvokeAddTroop();

                    }

                    transform.SetParent(GameManagerBattle.instance.objectHeroInGame.transform);

                    isGame = true;
                    
                }

                
            }
            else
            {
                SetMode(Mode.None);

                RemoveInGame();

                return;
            }  
        }

        switch (TypeControl)
        {
            case TypeHeroControl.Player:               
                if (mode != Mode.InGame && heroSel == this && changeTile)
                {
                    
                    SetMode(Mode.Selection);
                    ui.Setup();
                    ui.SetActive(true, true);
                    GetComponent<SelectionHero>().SelectionAction(true, false, isPlace);
                }
                SetMeshRender(true, false);
                break;
            case TypeHeroControl.None:
            case TypeHeroControl.CPU:
                if (mode != Mode.InGame)
                {
                    SetMode(Mode.InGame);
                    SetMeshRender(false, false);                    
                    GetComponent<SelectionHero>().SelectionAction(false, false, true);
                    ui.SetActive();
                }               
                break;
        }


        if (mode == Mode.Selection || mode == Mode.InGame)
        {
            RotationHero();

            animationHero.BoolAnimation("Empty", false);

            selection.ActivatedPlatform(false);
        }
            

    }

    private void RotationHero()
    {
        if (refSquad?.GetTeamId() % 2 == 0)
            transform.eulerAngles = new Vector3(0, 180, 0);
        else
            transform.eulerAngles = new Vector3(0, 0, 0);
    }

    public void RemoveInGame()
    {
        if (info.GetRarity() == Rarity.Hero)
        {
            DropInGame(true, tileGridStartPosition, this);
            GetComponent<SelectionHero>().SelectionAction(true, false, true);
            return;
        }

        ReturnToShop();
    }
    public void ReturnToShop()
    {
        ui.SetActive();

        if(tileGridCurrent)
            tileGridCurrent.SetOccupied(false, null);

        

        if (tileGridCurrent != null)
        {
            refSquad.InvokeRemoveTroop(this);
            
        }
        else
        {
            transform.SetParent(shopPos.transform);
            mode = Mode.Shop;
        }
        
        SetMeshRender(true,false);
        isGame = false;
        tileGridCurrent = null;
        tileGridStartPosition = null;
        animationHero.BoolAnimation("Empty", true);       
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        GetComponent<SelectionHero>().SelectionAction(false, false, false);
        GetComponent<SelectionHero>().ActivatedPlatform(true);
    }
    public void DragInGame(HeroMonster hero)
    {
        ActivetedParticle(false);

        if (hero == this)
        {
            ui.SetActive(false);

            if (mode == Mode.Shop)
                SetTile(null);

            SetMode(Mode.Drag);

            animationHero.BoolAnimation("Empty", true);
            SetMeshRender(true, false);
            return;
        }

        if (mode == Mode.Selection && hero.TypeControl == TypeControl)
            SetMeshRender(false, true);
        else
        {
            if(collisionComponent != null)
                collisionComponent.enabled = false;
        }
    }
    public void SetTileCurrent(TileGrid tile, bool change)
    {
        if (tile == null)
        {
            tile = GridManager.instance.GetNeigthbornTileEmpty(tileGridCurrent, refSquad.GetTeamId());

            if (tile == null)
                return;
        }        
        
        if (tileGridCurrent && !change && mode != Mode.InGame)
        {
            tileGridCurrent.SetOccupied(false, null);
        }

        tileGridStartPosition = tile;
        SetTile(tile);
        tile.SetOccupied(true, this);       

        ActivetedParticle(change);


        SetPosition(tileGridCurrent.transform.position);
    }

    public void ActivetedParticle(bool active)
    {
        if(particleChangePose != null)
            particleChangePose.gameObject.SetActive(active);
    }

    public TypeHeroControl FindEnemyControl()
    {
        Dictionary<TypeHeroControl, TypeHeroControl> control = new Dictionary<TypeHeroControl, TypeHeroControl>();
        TypeHeroControl typeHero = TypeHeroControl.None;
        control.Add(TypeHeroControl.Player, TypeHeroControl.CPU);
        control.Add(TypeHeroControl.CPU, TypeHeroControl.Player);

        if (control.TryGetValue(TypeControl, out typeHero))
        {
            return typeHero;
        }

        return TypeHeroControl.None;
    }

    protected void SetPreparetion()
    {
        info.Setup();
        death.Reset();

        if (mode == Mode.InGame)
        {
            ui.Setup();
            ui.SetActive(true, true);

            SetPosition(tileGridStartPosition.transform.position);
            
            switch(TypeControl)
            {
                case TypeHeroControl.Player:
                    transform.eulerAngles = new Vector3(0, 0, 0);                
                    break;
                case TypeHeroControl.CPU:
                    RotationHero();
                    break;
            }
            SetMeshRender(true, false);
            GetComponent<SelectionHero>().SelectionAction(false, false, true);
                
            animationHero.BoolAnimation("Run", false);
            animationHero.BoolAnimation("Empty", false);
            ChangeTile(tileGridStartPosition);
                
        }

        
    }
}
[System.Serializable]
public enum Mode
{
    None,
    Shop,
    Drag,
    Selection,
    InGame   
}

[System.Serializable]
public enum TypeHeroControl
{
    None,
    Player,
    CPU
}

[System.Serializable]

public enum BattleStateHero
{
    None,
    Move,
    Attack,
    Ability
}