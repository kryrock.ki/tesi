using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWinOrDefeat : MonoBehaviour
{
    [SerializeField] private Image imageWinOrLose;
    [SerializeField] private ChangeImage[] typeImage;

    private void Awake()
    {
        GameManagerBattle gb = GameObject.FindObjectOfType<GameManagerBattle>();
        gb.finishGame.AddListener(SetWinTeam);
    }

    public void SetWinTeam(int teamId)
    {
        Debug.Log(teamId);
        if (teamId % 2 == 0)
        {
            imageWinOrLose.sprite = ChooseImage(TypeImage.Lose);
        }
        else
        {
            imageWinOrLose.sprite = ChooseImage(TypeImage.Win);
        }
    }

    public Sprite ChooseImage(TypeImage typeImage)
    {
        Sprite sprite = null;

        foreach(ChangeImage image in this.typeImage) 
        {
            sprite = image.GetSprite(typeImage);

            if(sprite != null)
                return sprite;
        }

        return sprite;
    }
}

[System.Serializable]
public struct ChangeImage
{
    public TypeImage typeImage;
    public Sprite image;

    public Sprite GetSprite(TypeImage typeImage)
    {
        if(typeImage == this.typeImage)
            return this.image;

        return null;
    }

    
}

public enum TypeImage
{
    Win,
    Lose
}