using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTroop : ButtonScript
{
    public override void Activated()
    {
        if (GameManagerBattle.instance.CostMaxTroop <= GameManagerBattle.instance.tm.GetPlayerTeam().elisir)
        {
            GameManagerBattle.instance.tm.GetPlayerTeam().InvokeAddMaxTroop();
        }
    }

    public override void Deactivated()
    {
        throw new System.NotImplementedException();
    }
}
