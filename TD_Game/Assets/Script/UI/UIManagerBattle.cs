using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManagerBattle : MonoBehaviour
{
    public static UIManagerBattle instance;
    private UIShop uiShop;
    public UIShop GetUIShop() => uiShop;
    private UIInfoMonster infoMonsterUI;

    private UIWinOrDefeat winOrLose;
    public GameObject pointUI;
    public UIInfoMonster GetUIInfoMonster() => infoMonsterUI;

    private void Start()
    {
        instance = this;

        uiShop = GetComponentInChildren<UIShop>();
        infoMonsterUI = GetComponentInChildren<UIInfoMonster>();
        winOrLose = GetComponentInChildren<UIWinOrDefeat>();
        
        infoMonsterUI.gameObject.SetActive(false);
        winOrLose.gameObject.SetActive(false);
        pointUI.gameObject.SetActive(false);
        
        GameManagerBattle.instance.startBattle.AddListener(() => ActiveInfoMonster(false));
    }

    public void ActiveInfoMonster(bool activated, HeroMonster hero = null)
    {
        infoMonsterUI.gameObject.SetActive(activated);

        infoMonsterUI.Setup(activated, hero);



        //if(hero != null)
        //{
        //    infoMonsterUI.Activated(hero);
        //}
    }



}
