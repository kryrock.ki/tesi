using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIInfoMonster : MonoBehaviour
{
    [SerializeField] private RectTransform canvasRef;
    [SerializeField] private RectTransform infoPosition;
    [SerializeField] private RectTransform backgruond;
    [SerializeField] private float locationY = 660;
    [SerializeField] private float spaceScreen = 20;

    [SerializeField] private RectTransform loadingRect;

    private HeroMonster hero;

    [SerializeField] private float maxTimer = 0.5f;

    [Header("Hero Info: ")]
    [SerializeField] private Text nameHero;
    [SerializeField] private Text healt;
    [SerializeField] private Text speedMove;
    [SerializeField] private Text range;
    [SerializeField] private Text target;
    [SerializeField] private Text damage;
    [SerializeField] private Text speedAttack;
    [SerializeField] private Text waitAttack;
    [SerializeField] private Text energyHit;
    [SerializeField] private Text energyDamage;
    [SerializeField] private Text energyMax;


    private bool isTimer;
    private bool isActiveTimer;
    public bool GetActivatedTimer() => isActiveTimer;
    private float timer;


    #region Event

    public UnityAction startTimerEvent;

    public void StartTimerInvoke() => startTimerEvent?.Invoke();

    public UnityAction endTimerEvent;

    #endregion

    private void Awake()
    {
        infoPosition.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (isTimer)
        {
            timer += Time.deltaTime;
            SetAnimationLoading();

            if (timer >= maxTimer)
            {
                if(hero != null)
                    Activated(hero);
                isTimer = false;
            }
        }

    }

    private void SetLoading(bool activated)
    {
        loadingRect.gameObject.SetActive(activated);

        if (!activated)
            return;
        loadingRect.GetComponentInChildren<Image>().fillAmount = 0;
        Vector2 location = Input.mousePosition / canvasRef.localScale.x;
        loadingRect.anchoredPosition = location;

    }

    private void SetAnimationLoading()
    {
        loadingRect.GetComponentInChildren<Image>().fillAmount = timer / maxTimer;
    }

    public void Activated(HeroMonster hero)
    {
        SetLoading(false);
        infoPosition.gameObject.SetActive(true);

        SetAllText(hero);

        Vector2 heroLocalLocation = Camera.main.WorldToScreenPoint(hero.transform.position);
        Vector2 location = (new Vector3(heroLocalLocation.x, heroLocalLocation.y, 0) / canvasRef.localScale.x) + new Vector3(0, locationY);

        if (location.x + (backgruond.rect.width / 2) > canvasRef.rect.width)
        {
            location.x = (canvasRef.rect.width - (backgruond.rect.width / 2)) - spaceScreen;
        }

        if (location.x - (backgruond.rect.width / 2) < 0)
        {
            location.x = (backgruond.rect.width / 2) + spaceScreen;
        }
        if (location.y + (backgruond.rect.height / 2) > canvasRef.rect.height)
        {

            location.y = (canvasRef.rect.height - (backgruond.rect.height / 2)) - spaceScreen;
        }

        if (location.y - (backgruond.rect.height / 2) < 0)
        {
            location.y = (backgruond.rect.height / 2) + spaceScreen;
        }



        infoPosition.anchoredPosition = location;

        isTimer = false;
        isActiveTimer = false;
        //hero.transform.position - new Vector3(0, (hero.transform.position.y - (startLocation.y - distance)), 0)
        //Debug.Log(hero.transform.position.ToString() + " / " + distance + " / " + startLocation);
        //Debug.Log(heroLocalLocation + "   /   " + Input.mousePosition);
        //backgroud.transform.position += new Vector3(0,locationY, 0);
    }

    public void SetAllText(HeroMonster hero)
    {
        nameHero.text = hero.info.GetHeroObject().GetName();
        healt.text = hero.info.GetHealtMax().ToString();
        speedMove.text = hero.info.GetMovimentSpeed().ToString();
        range.text = hero.info.GetAttackBaseInfo().GetRangeCell().ToString();
        target.text = hero.info.GetAttackBaseInfo().GetRangeAttack().ToString();
        damage.text = hero.info.GetAttackBaseInfo().GetDamage().ToString();
        speedAttack.text = hero.info.GetAttackBaseInfo().GetAttackSpeed().ToString();
        waitAttack.text = hero.info.GetAttackBaseInfo().GetAttackWaiting().ToString();
        energyHit.text = hero.info.GetInfoHero().GetIncrementSuper().ToString();
        energyDamage.text = hero.info.GetInfoHero().GetDamageIncrementSuper().ToString(); ;
        energyMax.text = hero.info.GetInfoHero().GetMaxSuper().ToString();
    }

    public void Setup(bool timerValue, HeroMonster hero)
    {
        timer = 0;
        infoPosition.gameObject.SetActive(false);
        this.hero = hero;
        isActiveTimer = timerValue;

        if (timerValue)
        {
            StartCoroutine("SetTimer");
        }
        else
        {
            StopCoroutine("SetTimer");
        }
        
        
    }

    private IEnumerator SetTimer()
    {
        isTimer = false;
        SetLoading(false);
        yield return new WaitForSecondsRealtime(0.3f);
        StartTimerInvoke();
        GridManager.instance.SetAllSelectionTile(TileSelection.None);
        isTimer = true;
        SetLoading(true);
    }
}
