using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonRefull : ButtonScript
{
    public override void Activated()
    {
        Squad teamPlayer = GameManagerBattle.instance.tm.GetPlayerTeam();
        if (teamPlayer != null && teamPlayer.shuffleCount > 0)
        {
            teamPlayer.ShopRefull();
            teamPlayer.RemoveShuffle(1);

        }
    }

    public override void Deactivated()
    {
        throw new System.NotImplementedException();
    }

}
