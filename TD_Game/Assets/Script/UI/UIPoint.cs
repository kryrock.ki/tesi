using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPoint : MonoBehaviour
{
    private int id = 1;
    [SerializeField] private GameObject pointWin;

    private void Start()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
        SetActivated(false);
    }

    public void Setup(int id)
    {
        SetActivated(false);
 
    }

    private void SetActivated(bool Activated)
    {
        pointWin.SetActive(Activated);
    }

    public void ActivatedPoint()
    {
        SetActivated(true);
    }
}
