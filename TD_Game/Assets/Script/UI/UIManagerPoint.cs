using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManagerPoint : MonoBehaviour
{
    private GameManagerBattleSingle gb;
    [SerializeField] private GameObject pointWin;
    [SerializeField] private GameObject pointContainer;
    [SerializeField] private TypeHeroControl control;
    [SerializeField] private UIPoint[] pointsWin;
    private int point;
    // Start is called before the first frame update
    private void Awake()
    {
        gb = GameObject.FindObjectOfType<GameManagerBattleSingle>();
        if(gb != null)
            SpawnPointWin();


    }

    private void SpawnPointWin()
    {
        pointsWin = new UIPoint[gb.points.GetMaxPoint()];

        for (int i = 0; i < gb.points.GetMaxPoint(); i++)
        {
            pointsWin[i] = Instantiate(pointWin).GetComponent<UIPoint>();

            if (pointsWin[i] != null)
            pointsWin[i].gameObject.transform.SetParent(pointContainer.transform);
        }
    }

    public void AddPoint()
    {
        switch(control)
        {
            case TypeHeroControl.CPU:
                ControlPoint(gb.points.GetPointEnemy());
                break;
            case TypeHeroControl.Player:
                ControlPoint(gb.points.GetPointPlayer());
                break;
        }
    }

    private void ControlPoint(int point)
    {
        if (point > this.point && pointsWin.Length - 1 >= point - 1)
        {
            pointsWin[point - 1].ActivatedPoint();
            this.point = point;
        }
    }
}
