using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIShop : MonoBehaviour
{
    private Squad teamPlayer; 
    
    public TargetShop[] positionShop;

    public Text textUnit;
    public Text textCostUnit;

    #region Event

    public UnityEvent SpawnChest;
    public void InvokeSpawChest() => SpawnChest.Invoke();
    #endregion

    private void Start()
    {
        if (GameManagerBattle.instance.tm.isPlayer)
        {
            teamPlayer = GameManagerBattle.instance.tm.GetPlayerTeam();
            teamPlayer.AddTroopEvent.AddListener(() => StartCoroutine(SetTextUnit()));
            teamPlayer.RemoveTroopEvent.AddListener((hero) => StartCoroutine(SetTextUnit()));
            teamPlayer.AddMaxTroopEvent.AddListener(() => StartCoroutine(SetTextUnit()));
            StartCoroutine(SetTextUnit());
        }
    }

    private IEnumerator SetTextUnit()
    {
        yield return new WaitForSeconds(0.1f);
        textUnit.text = teamPlayer.GetCountTroop()  + "/" + teamPlayer.GetMaxTroop();
        textCostUnit.text = GameManagerBattle.instance.CostMaxTroop.ToString();
    }

    public void ActiveAnimationChest()
    {
        if (GameManagerBattle.instance.tm.isPlayer)
        {
            InvokeSpawChest();
            return;
        }

        GameManagerBattle.instance.InvokStartPreparetionHero();
    }
}
