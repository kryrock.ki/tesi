using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBattle : ButtonScript
{
    public override void Activated()
    {
        GameManagerBattle.instance.tm.GetPlayerTeam().FinishPreparetion();
        GridManager.instance.SetAllSelectionTile(TileSelection.None);
    }

    public override void Deactivated()
    {
        throw new System.NotImplementedException();
    }

}
