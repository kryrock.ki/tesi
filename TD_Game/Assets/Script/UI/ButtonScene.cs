using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScene : ButtonScript
{
    public override void Activated()
    {
        
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    public override void Deactivated()
    {
        throw new System.NotImplementedException();
    }

}
