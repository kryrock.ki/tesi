using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TargetShop : MonoBehaviour
{
    [SerializeField] private GameObject elisirImage;
    [SerializeField] private TextMeshProUGUI elisirText;


    // Start is called before the first frame update
    void Start()
    {
        SetActivated(false);
        SetRotationImage(elisirImage);
    }

    private void SetActivated(bool image)
    {
        elisirImage.SetActive(image);
    }

    private void SetText(int cost)
    {
        elisirText.text = cost.ToString();
    }

    private void SetRotationImage(GameObject gameObject)
    {
        gameObject.transform.eulerAngles = new Vector3(Camera.main.transform.eulerAngles.x, gameObject.transform.eulerAngles.y - gameObject.transform.eulerAngles.y, 0);
    }

    public void SetVisibleInfo(bool visible = false, int cost = 0)
    {
        SetActivated(visible);
        SetText(cost);
    }

}
