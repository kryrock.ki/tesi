using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Team
{
    public GameObject heroSpecial;

    public GameObject[] heroes = new GameObject[4];
}
