using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAction : MonoBehaviour
{
    public static PlayerAction Instance { get; private set; }
    public UnityAction<TypeHeroControl, HeroMonster> DragHeroEvent;
    public void DragHeroInvok(TypeHeroControl typeHero, HeroMonster hero) => DragHeroEvent.Invoke(typeHero, hero);

    public UnityAction<bool, TileGrid, HeroMonster> DropHeroEvent;
    public void DropHeroInvok(bool isPlace, TileGrid tile = null, HeroMonster hero = null) => DropHeroEvent.Invoke(isPlace, tile, hero);

    public UnityAction<HeroMonster> clickHeroEvent;
    public void ClickHeroInvok(HeroMonster hero) => clickHeroEvent?.Invoke(hero);


    private void Awake()
    {
        Instance = this;
    }
}
