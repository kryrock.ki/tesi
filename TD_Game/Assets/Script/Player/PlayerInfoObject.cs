using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="PayerInfo", menuName = "Custom/PlayerInfo", order = 1)]
public class PlayerInfoObject : ScriptableObject
{
    public Team team;
}
