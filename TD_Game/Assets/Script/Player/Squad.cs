using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Squad : MonoBehaviour
{
    public TypeHeroControl typeControl;
    public PlayerInfoObject PlayerInfo;
    public AllHeroObject allHero;
    public GameObject[] team;
    public GameObject[] shopHero;
    private ObjectPool[] objectPool;
    public int teamId;
    public int GetTeamId() => teamId;
    private Color colorTeam;
    public Color GetColor() => colorTeam;
    public int elisir;
    public int shuffleCount;
    private bool isFinish = false;
    public bool GetFinish() => isFinish;
    [SerializeField] private int maxTroop;
    public int GetMaxTroop() => maxTroop;

    public void AddMaxTroop() => maxTroop++;

    [SerializeField] private int countTroop;
    public int GetCountTroop() => countTroop;

    public void AddTroop() => countTroop++;
    public void RemoveTroopCount() => countTroop--;
    public bool PossibleAddTroop() => countTroop < maxTroop;
    #region Event

    public UnityEvent<int> changeElisir;
    public void InvokeChangeElisir(int elisir) => changeElisir.Invoke(elisir);
    
    public UnityEvent<int> changeShuffle;
    public void InvokeChangeShuffle(int elisir) => changeShuffle.Invoke(elisir);

    public UnityEvent AddTroopEvent;
    public void InvokeAddTroop() => AddTroopEvent?.Invoke();
    public UnityEvent<HeroMonster> RemoveTroopEvent;
    public void InvokeRemoveTroop(HeroMonster hero) => RemoveTroopEvent?.Invoke(hero);
    public UnityEvent AddMaxTroopEvent;
    public void InvokeAddMaxTroop() => AddMaxTroopEvent?.Invoke();

    #endregion

    private IEnumerator Start()
    {
        Setup(GameManagerBattle.instance.totShop, GameManagerBattle.instance.startElisir, 6);

        objectPool = new ObjectPool[PlayerInfo.team.heroes.Length];

        for(int i = 0; i < objectPool.Length; i++)
        {
            objectPool[i] = new ObjectPool(1, PlayerInfo.team.heroes[i], Vector3.zero, transform);
        }

        yield return new WaitForSecondsRealtime(3.5f);
        SetSpecialHero();

    }

    public void SettingTeam(int teamId, Color color)
    {
        this.teamId = teamId;
        colorTeam = color;
    }

    public void Setup(int shop, int elisir, int team)
    {
        this.team = new GameObject[team];
        shopHero = new GameObject[shop];
        this.elisir = elisir;
        maxTroop = GameManagerBattle.instance.MaxTroop;
        shuffleCount = GameManagerBattle.instance.startShuffle;
        SetTeam();

        AddTroopEvent.AddListener(AddTroop);
        RemoveTroopEvent.AddListener(RemoveTroop);
        AddMaxTroopEvent.AddListener(AddMaxTroop);
        AddMaxTroopEvent.AddListener(() => RemoveElisr(GameManagerBattle.instance.CostMaxTroop));
        GameManagerBattle.instance.startPraparetionHero.AddListener(ChooseHero);
        GameManagerBattle.instance.troopPreparetionGrid.AddListener(AddTurn);

    }


    private void SetTeam()
    {
        switch (typeControl)
        {
            case TypeHeroControl.Player:
                team[0] = PlayerInfo.team.heroSpecial;
                for (int i = 0; i < PlayerInfo.team.heroes.Length; i++)
                {
                    team[i + 1] = PlayerInfo.team.heroes[i];
                }
                break;
            case TypeHeroControl.CPU:
                for (int i = 0; i < team.Length; i++)
                {
                    if (i == 0)
                        team[i] = allHero.GetAllHeroSpecial()[Random.Range(0, allHero.GetAllHeroSpecial().Length)];
                    else
                        team[i] = allHero.GetAllHero()[Random.Range(0, allHero.GetAllHero().Length)];
                }
                break;
        }
        
    }


    public void ChooseHero()
    {

        StartCoroutine(RefullAndPlaceHero());


    }

    private IEnumerator RefullAndPlaceHero()
    {
        ShopRefull();

        yield return new WaitForSeconds(2);

        if (typeControl == TypeHeroControl.CPU)
        {
            StartCoroutine(AddHeroInGame());

            yield return new WaitForSeconds(2);

            FinishPreparetion();
        }


    }

    public void FinishPreparetion()
    {
        isFinish = true;

        GameManagerBattle.instance.StartBattleInvoke();
    }

    private void AddTurn()
    {
        isFinish = false;

        AddElisir(GameManagerBattle.instance.addAllTurnElisir);
        AddShuffle(GameManagerBattle.instance.addAllTurnShuffle);
    }

    public void AddElisir(int elisir)
    {
        this.elisir = this.elisir + elisir;

        this.elisir = Mathf.Clamp(this.elisir, 0, GameManagerBattle.instance.MaxElisir);

        InvokeChangeElisir(this.elisir);

    }

    public void AddShuffle(int shuffle)
    {
        shuffleCount += shuffle;

        shuffleCount = Mathf.Clamp(shuffleCount, 0, GameManagerBattle.instance.maxShuffle);

        InvokeChangeShuffle(shuffleCount);

    }

    public void RemoveShuffle(int shuffle)
    {

        shuffleCount = Mathf.Clamp(shuffleCount - shuffle, 0, GameManagerBattle.instance.maxShuffle);

        InvokeChangeShuffle(shuffleCount);

    }

    public void RemoveElisr(int elisirRemove)
    {
        elisir = Mathf.Clamp(elisir - elisirRemove, 0, GameManagerBattle.instance.MaxElisir);

        InvokeChangeElisir(elisir);
    }

    private void RemoveShopPos()
    {
        if(typeControl == TypeHeroControl.Player) 
        {
            foreach (GameObject shop in shopHero)
            {
                if (shop != null && !shop.GetComponent<HeroMonster>().isGame)
                {
                    shop.SetActive(false);
                }
            }
        }
    }
    public void ShopRefull()
    {
        int index = shopHero.Length;
        int current = shopHero.Length;

        RemoveShopPos();


        for (int i = 0; i < shopHero.Length; i++)
        {

            while (index == current)
            {
                index = Random.Range(1, team.Length - 1);
            }

            //shopHero[i] = PlayerInfo.team.heroes[index];

            switch(typeControl)
            {
                case TypeHeroControl.CPU:
                    shopHero[i] = team[index];                   
                    break;
                case TypeHeroControl.Player:

                    GameObject hero = GetObjectInstance(team[index]);

                    if (hero == null)
                        return;
 
                    //Instantiate(shopHero[i]);
                    shopHero[i] = hero;            
                    hero.SetActive(true);
                    TargetShop shop = UIManagerBattle.instance.GetUIShop().positionShop[i];

                    shop.SetVisibleInfo(true,hero.GetComponent<HeroMonster>().info.GetInfoHero().GetCost());
                    hero.GetComponent<HeroMonster>().Setup(TypeHeroControl.Player,this, shop.gameObject);
                    hero.GetComponent<HeroMonster>().ReturnToShop();

                    
                    break;

            }

            current = index;
        }

    }

    private IEnumerator AddHeroInGame()
    {
        if (!GridManager.instance.ControlTileEmpty(teamId))
            yield break;

        if (countTroop >= maxTroop)
        {
            if (elisir >= GameManagerBattle.instance.CostMaxTroop)
            {
                InvokeAddMaxTroop();
                yield return new WaitForSecondsRealtime(0.2f);
            }
            else
                yield break;


        }


        HeroMonster hero = null;

        int cost = 0;
        int refull = 0;

        while (refull < shuffleCount)
        {
            for (int i = 0; i < shopHero.Length; i++)
            {
                hero = shopHero[i].GetComponent<HeroMonster>();
                cost = hero.info.GetInfoHero().GetCost();

                if (elisir < 1)
                    break;

                //List<TileGrid> tiles= new List<TileGrid>();
                //
                //switch (hero.info.GetInfoHero().GetTypeHeroAttack())
                //{
                //    case TypeHeroAttack.Meele:
                //        tiles = GridManager.instance.GetTileMelee(teamId);
                //        break;
                //    case TypeHeroAttack.Range:
                //        tiles = GridManager.instance.GetTileRange(teamId);
                //        break;
                //}
                //
                //
                //

                TileGrid tile = ChooseTile(hero);

                if (tile == null)
                       continue;


                if (cost <= elisir && countTroop < maxTroop)
                {
                    HeroMonster heroMonsters = Instantiate(hero.gameObject, GameManagerBattle.instance.objectHeroInGame.transform).GetComponent<HeroMonster>();
                    heroMonsters.Setup(TypeHeroControl.CPU, this);

                    heroMonsters.DropInGame(true, tile, heroMonsters);
                }
            }

            if (elisir > 0)
            {
                ShopRefull();
                RemoveShuffle(1);
                refull++;
            }

            if (elisir < 1)
                break;
        }

    }


    private void SetSpecialHero()
    {
        HeroMonster hero = null;
        TileGrid tile = null; 

        hero = Instantiate(team[0], GameManagerBattle.instance.objectHeroInGame.transform).GetComponent<HeroMonster>();


        switch (typeControl)
        {
            case TypeHeroControl.Player:
                tile = GridManager.instance.GetCenterCamp(teamId);                            
                hero.Setup(TypeHeroControl.Player, this);
                hero.DropInGame(true, tile, hero);
                
                //hero.SetPosition(tile.transform.position);
                //hero.ChangeTile(tile);
                break;
            case TypeHeroControl.CPU:
                tile = ChooseTile(hero.GetComponent<HeroMonster>());
                hero.Setup(TypeHeroControl.CPU, this);
                hero.DropInGame(true, tile, hero);
                break;
        }

    }

    public void RemoveTroop(HeroMonster hero)
    {
        hero.gameObject.transform.parent = transform;
        hero.gameObject.SetActive(false);
        AddElisir(hero.info.GetInfoHero().GetCost());
        RemoveTroopCount();
    }

    private GameObject GetObjectInstance(GameObject gameObject)
    {
        foreach (ObjectPool ob in objectPool)
        {
            if (ob.GetAssetPath() == gameObject)
            {
                return ob.Get();
            }
        }

        return null;
    }

    private TileGrid ChooseTile(HeroMonster hero)
    {
        List<TileGrid> tiles = new List<TileGrid>();

        switch (hero.info.GetInfoHero().GetTypeHeroAttack())
        {
            case TypeHeroAttack.Archer:
            case TypeHeroAttack.Wizard:
            case TypeHeroAttack.SupportRange:
                tiles = new List<TileGrid>(GridManager.instance.GetTileRange(teamId));
                break;
            case TypeHeroAttack.Tank:
            case TypeHeroAttack.SupportMeele:
            case TypeHeroAttack.murderer:
                tiles = new List<TileGrid>(GridManager.instance.GetTileMelee(teamId));
                break;
        }

        if (tiles.Count == 0)
            return null;

        int index = Random.Range(0, tiles.Count);
        return tiles[index];
    }
}
