using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR

[CustomEditor(typeof(MeshEditorScript))]
public class EditMaterial : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
    
        //DrawDefaultInspector();
        MeshEditorScript edit = (MeshEditorScript)target;
    
        if (GUILayout.Button("Change Color"))
        {
            edit.ChangeColorMaterial();
        }
    
    }
}

#endif
