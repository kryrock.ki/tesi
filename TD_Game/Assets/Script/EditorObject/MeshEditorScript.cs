using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshEditorScript : MonoBehaviour
{
    [SerializeField] private Color colorBase;

    public void ChangeColorMaterial()
    {
        GetComponent<MeshRenderer>().material.color = colorBase;
    }
}
