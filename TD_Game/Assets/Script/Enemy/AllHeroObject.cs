using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AllHero", menuName = "Custom/AllHero", order = 1)]
public class AllHeroObject : ScriptableObject
{
    [SerializeField] private GameObject[] allHeroSpecial;
    public GameObject[] GetAllHeroSpecial() => allHeroSpecial;

    [SerializeField] private GameObject[] allHero;
    public GameObject[] GetAllHero() => allHero;
}
