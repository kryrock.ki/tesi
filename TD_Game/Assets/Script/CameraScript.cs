using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private Vector3 startLocation;
    [SerializeField] private Quaternion startRotation;
    [SerializeField] private Vector3 battleLocation;
    [SerializeField] private Quaternion battleRotation;
    private GameObject refPosAndRot;
    [SerializeField] private float SpeedAnimation;

    void Start()
    {
        startLocation = transform.position;
        startRotation = transform.rotation;

        battleLocation = transform.GetChild(0).transform.position;
        battleRotation = transform.GetChild(0).transform.rotation;

        GameManagerBattle.instance.startBattle.AddListener(() => StartCoroutine(Move(battleLocation, battleRotation, SpeedAnimation)));
        GameManagerBattle.instance.troopPreparetionGrid.AddListener(() => StartCoroutine(Move(startLocation, startRotation, SpeedAnimation)));
    }

    private IEnumerator Move(Vector3 location, Quaternion rotation, float duration)
    {
        transform.DOMove(location, duration);
        transform.DORotateQuaternion(rotation, duration);
        yield return new WaitForSeconds(duration + 0.05f);
        gameObject.transform.position = location;
        gameObject.transform.rotation = rotation;
    }
}
