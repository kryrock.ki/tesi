using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiagonalBulletTarget : TypeBulletAttack
{
    public DiagonalBulletTarget(TypeEffect typeConditionAllTile, TypeEffect typeConditionCurrentTile, ConditionAttack condition)
    {
        this.typeConditionAllTile = typeConditionAllTile;
        this.typeConditionCurrentTile = typeConditionCurrentTile;
        this.condition = condition;
    }

    public override void Attack(HeroMonster hero, HeroMonster target, int damage, int range)
    {
        if (target == null)
            return;
        TileGrid[] tileNeighborn = AllTileTarget(target.tileGridCurrent, range);
        List<HeroMonster> enemys = new List<HeroMonster>();
        List<HeroMonster> players = new List<HeroMonster>();

        enemys.Add(target);
        players.Add(hero);

        foreach (TileGrid tile in tileNeighborn)
        {
            tile.GetConditionParticle().ActiveParticle(typeConditionAllTile);

            if (tile.GetOccupated() && tile.GetHero() != hero)
            {
                if (tile.GetHero().TypeControl != hero.TypeControl)
                {
                    if (tile.GetHero() == target)
                        target.Damage(damage, hero);

                    if (!enemys.Contains(tile.GetHero()))
                    {
                        tile.GetHero().Damage(damage, hero);
                        enemys.Add(tile.GetHero());
                    }
                }
                else if(!players.Contains(tile.GetHero()))
                {
                    players.Add(tile.GetHero());
                }
            }
        }

        target.Damage(damage, hero);

        target.tileGridCurrent.GetConditionParticle().ActiveParticle(typeConditionCurrentTile);

        SetCondition(enemys.ToArray(), players.ToArray());

    }

    public TileGrid[] AllTileTarget(TileGrid tileCurrent, int rangeMax)
    {
        List<TileGrid> tileneighborn = new List<TileGrid>();
        List<TileGrid> control = new List<TileGrid>();
        List<Vector3> direction = new List<Vector3>();
        
        tileneighborn.AddRange(tileCurrent.GetNeighbornDiagonal());

        int range = 1;

        foreach (TileGrid tile in tileneighborn)
        {
            direction.Add(tile.transform.position - tileCurrent.transform.position);
        }

        while (range < rangeMax + 1)
        {

            foreach (TileGrid tile in tileneighborn)
            {
                control.AddRange(tile.GetNeighbornDiagonal());
            }

            foreach (TileGrid tile in control)
            {
                if (direction.Contains((tile.transform.position - tileCurrent.transform.position) / range))
                {                   
                    if (!tileneighborn.Contains(tile))
                        tileneighborn.Add(tile);
                }
            }

            control.Clear();
            range++;
        }

        return tileneighborn.ToArray();
    }

    public override void SetCondition(HeroMonster[] targetsEnemy = null, HeroMonster[] targetsPlayer = null)
    {
        if (condition == null || condition.conditionObject.conditionSetup.GetTypeCondition() == TypeCondition.None)
            return;

        switch (condition.GetTypeTargetCondition())
        {
            case TypeTargetCondition.AllTargetEnemy:
                foreach (HeroMonster hero in targetsEnemy)
                {
                    hero.condition.AddCondition(condition.conditionObject);
                }
                break;
            case TypeTargetCondition.TargetEnemy:
                targetsEnemy[0].condition.AddCondition(condition.conditionObject);
                break;
            case TypeTargetCondition.AllTargetPlayer:
                foreach (HeroMonster hero in targetsPlayer)
                {
                    hero.condition.AddCondition(condition.conditionObject);
                }
                break;
            case TypeTargetCondition.SelfPlayer:
                targetsPlayer[0].condition.AddCondition(condition.conditionObject);
                break;

        }
    }
}
