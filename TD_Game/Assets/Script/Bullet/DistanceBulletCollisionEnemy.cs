using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DistanceBulletCollisionEnemy : RangeBullet
{
    public Ease easeType;
    public float duration;
    public float JumpRange;

    private IEnumerator Moving()
    {
        TweenParams param = new TweenParams().SetEase(easeType);
        if(target)
            transform.DOJump(target.transform.localPosition, JumpRange, 1, duration, false);
        else
            transform.DOJump(heroSelf.transform.forward * 8, JumpRange, 1, duration, false);
        yield return new WaitForSeconds(duration);
        transform.position = target.transform.localPosition;
        gameObject.SetActive(false);

    }

    public override void Attack()
    {
        StartCoroutine(Moving());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponentInParent<HeroMonster>())
        {
            HeroMonster hero = other.transform.GetComponentInParent<HeroMonster>();

            if (hero.TypeControl != heroSelf.TypeControl)
            {
                bulletAttack.Attack(heroSelf, hero, damage, attackBase.GetRangeAttack());
                gameObject.SetActive(false);
            }
        }
    }
}
