using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RangeBullet : MonoBehaviour
{
    public TypeAttackBullet attackBullet;
    public ConditionAttack condition;
    public TypeEffect typeConditionAllTile;
    public TypeEffect typeConditionCurrentTile;
    [HideInInspector] public InfoAttack attackBase;
    [HideInInspector] public TypeBulletAttack bulletAttack;
    [HideInInspector] public TypeHeroControl typeHero;
    [HideInInspector] public HeroMonster heroSelf;
    [HideInInspector] public GameObject target;
    [HideInInspector] public TileGrid tileTarget;
    [HideInInspector] public int damage;

    private void Start()
    {
        switch (attackBullet)
        {
            case TypeAttackBullet.SingleTarget:
                bulletAttack = new SingleBulletTarget(typeConditionAllTile, typeConditionCurrentTile, condition);
                break;
            case TypeAttackBullet.AQuadrato:
                bulletAttack = new QuadratoBulletTarget(typeConditionAllTile, typeConditionCurrentTile, condition);
                break;
            case TypeAttackBullet.ACroce:
                bulletAttack = new CroceBulletTarget(typeConditionAllTile, typeConditionCurrentTile, condition);
                break;
            case TypeAttackBullet.ARombo:
                bulletAttack = new RomboBulletTarget(typeConditionAllTile, typeConditionCurrentTile, condition);
                break;
            case TypeAttackBullet.Diagonalmente:
                bulletAttack = new DiagonalBulletTarget(typeConditionAllTile, typeConditionCurrentTile, condition);
                break;
        }
    }

    public void Setup(HeroMonster heroSelf, int damage, InfoAttack attackBase, GameObject heroTarget = null, TileGrid tile = null)
    {
        transform.position = heroSelf.transform.position;
        gameObject.SetActive(true);
        this.heroSelf = heroSelf;
        this.typeHero = heroSelf.TypeControl;
        target = heroTarget;
        tileTarget = tile;
        this.damage = damage;
        this.attackBase = attackBase;
        Attack();

    }

    public abstract void Attack();

}
