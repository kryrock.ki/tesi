using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DistanceBulletTargetEnemy : RangeBullet
{
    public Ease easeType;
    public float duration;
    public float JumpRange;

    private IEnumerator Moving()
    {
        TweenParams param = new TweenParams().SetEase(easeType);
        transform.DOJump(target.transform.position, JumpRange, 1, duration, false);
        yield return new WaitForSeconds(duration);
        transform.position = target.transform.position;
        if(target.GetComponent<HeroMonster>())
            bulletAttack.Attack(heroSelf, target.GetComponent<HeroMonster>(), damage, attackBase.GetRangeAttack());
        gameObject.SetActive(false);
    }

    public override void Attack()
    {
        StartCoroutine(Moving());
    }
}
