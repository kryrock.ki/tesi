using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TypeBulletAttack
{
    public int damage;
    public TypeEffect typeConditionAllTile;
    public TypeEffect typeConditionCurrentTile;
    public ConditionAttack condition;
    public abstract void Attack(HeroMonster hero, HeroMonster target, int damage, int range);

    public abstract void SetCondition(HeroMonster[] targetsEnemy = null, HeroMonster[] targetsPlayer = null);
}
[System.Serializable]
public enum TypeAttackBullet
{
    SingleTarget,
    ACroce,
    ARombo,
    Diagonalmente,
    AQuadrato
}