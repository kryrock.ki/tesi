using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleBulletTarget : TypeBulletAttack
{
    public SingleBulletTarget(TypeEffect typeConditionAllTile, TypeEffect typeConditionCurrentTile, ConditionAttack condition)
    {
        this.typeConditionAllTile = typeConditionAllTile;
        this.typeConditionCurrentTile = typeConditionCurrentTile;
        this.condition = condition;
    }

    public override void Attack(HeroMonster hero, HeroMonster target, int damage, int range)
    {
        if (target == null)
            return;

        List<HeroMonster> enemys = new List<HeroMonster>();
        List<HeroMonster> player = new List<HeroMonster>();

        enemys.Add(target);
        player.Add(hero);

        target.Damage(damage, hero);

        target.tileGridCurrent.GetConditionParticle().ActiveParticle(typeConditionCurrentTile);

        SetCondition(enemys.ToArray(), player.ToArray());

    }

    public override void SetCondition(HeroMonster[] targetsEnemy = null, HeroMonster[] targetsPlayer = null)
    {
        if (condition.conditionObject == null || condition.conditionObject.conditionSetup.GetTypeCondition() == TypeCondition.None)
            return;

        switch (condition.GetTypeTargetCondition())
        {
            case TypeTargetCondition.AllTargetEnemy:
            case TypeTargetCondition.TargetEnemy:
                targetsEnemy[0].condition.AddCondition(condition.conditionObject);
                break;
            case TypeTargetCondition.AllTargetPlayer:
            case TypeTargetCondition.SelfPlayer:
                targetsPlayer[0].condition.AddCondition(condition.conditionObject);
                break;

        }
    }
}
