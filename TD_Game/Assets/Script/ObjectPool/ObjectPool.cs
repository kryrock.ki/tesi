using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//deve restituire un oggetto disponibile nella catena
public class ObjectPool
{
    private uint _stackSize;
    private GameObject _assetPath;
    public GameObject GetAssetPath() { return _assetPath; }
    private List<GameObject> _data;
    private Vector3 PositionSpawn;
    private Transform transformPos;
    private bool shouldIncreaseStackSize => _data.Count - GetActiveElementsCount() == 1;
    public ObjectPool(uint initStackSize, GameObject assetPath, Vector3 pos, Transform trans)
    {
        _stackSize = initStackSize;
        _assetPath = assetPath;
        PositionSpawn = pos;
        transformPos = trans;
        _data = new List<GameObject>();
       
        // genera stack pool
        for(uint i = 0; i < initStackSize; i++)
        {
            GameObject item = Object.Instantiate(assetPath, trans);
            item.transform.localPosition = pos;
            item.SetActive(false);

            _data.Add(item);
        
        }


    }

    public GameObject Get() 
    {
        GameObject result = GetFirstAvailabElement(out bool elemenyFound);

        if(elemenyFound)
        {
            //sarebbe il caso di aumentare la dimensione della lista?
            if(shouldIncreaseStackSize)
            {
               IncreaseStackSize(_stackSize);
            }
            return result;
        }
        return null;
    }

    private GameObject GetFirstAvailabElement(out bool hasSuccess)
    {
        foreach(GameObject element in _data)
        {
            if(!element.activeSelf)
            {
                hasSuccess = true;
                return element;
            }
        }

        hasSuccess = false;
        return null;
    }

    private uint GetActiveElementsCount()
    {
        uint result = 0;

        foreach (GameObject element in _data)
        {
            if(element.activeSelf)
            {
                result++;
            }
            
        }

        return result;
        
    }

    void IncreaseStackSize(uint increment)
    {
        for(uint i = 0; i < increment; i++)
        {
            var item = Object.Instantiate(_assetPath, transformPos);
            item.transform.localPosition = PositionSpawn;
            item.SetActive(false);

            _data.Add(item);
        }
    }
}
