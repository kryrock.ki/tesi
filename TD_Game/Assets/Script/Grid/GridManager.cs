using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public class GridManager : MonoBehaviour
{
    public static GridManager instance;

    [SerializeField] private GridObject gridData;
    public GridObject GetGridData() => gridData;
    [SerializeField] private GameObject gridSelectionView;
    public float GetSpaceTile() => gridData.GetSpace();

    public List<TileGrid> tileGrieds;

    [SerializeField] private GridView[] gridView;

    #region Event
    public UnityAction clearTileEvent;

    public void ClearTileInvoke() => clearTileEvent?.Invoke();


    #endregion
    // Start is called before the first frame update
    void Awake()
    {
        Debug.Log("Awake");

        GameObject.FindObjectOfType<PlayerAction>().DragHeroEvent += (typeHero, hero) => ActivatedGridView(true, typeHero);
        GameObject.FindObjectOfType<PlayerAction>().DropHeroEvent += (isPlace, drop, tile) => ActivatedGridView(false);

        gridView = new GridView[2];

        instance = this;
        CreateGrid();


    }

    private void ActivatedGridView(bool activated, TypeHeroControl hero = TypeHeroControl.None)
    {
        foreach (GridView grid in gridView)
        {
            if (grid != null)
            {
                grid.gameObject.SetActive(activated);
        
                if (grid.enabled)
                    grid.IsActive(hero);
            }
            
        }
    }

    public void CreateGrid()
    {
        tileGrieds = new List<TileGrid>();

        int childCount = transform.childCount;

        for (int i = 0; i < childCount; i++)
        {
            DestroyImmediate(gameObject.transform.GetChild(0).gameObject);
        }

        int tileIndex = 0;
        
        for (int i = 0; i < gridData.GetSize().y; i++)
        {
            // instantiate tile
            for (int j = 0; j < gridData.GetSize().x; j++)
            {
                tileIndex++;
        
                GameObject tile = Instantiate(gridData.GetTile(), transform);
                tile.transform.localPosition = (new Vector3(j, 0, i) + gridData.GetOrigin()) *
                                          gridData.GetSpace();
                Vector3 scaleSize = tile.transform.localScale;
                tile.transform.localScale = new Vector3(gridData.GetTileSize().x * scaleSize.x, 1, gridData.GetTileSize().y * scaleSize.z);               

                if(tileIndex <= ((gridData.GetSize().x * gridData.GetSize().y) / 2))
                    tile.GetComponent<TileGrid>().Initialize(tileIndex, gridData.GetTileSize(), TileType.Player);
                else
                    tile.GetComponent<TileGrid>().Initialize(tileIndex, gridData.GetTileSize(), TileType.Enemy);

                tile.gameObject.name = "tile " + tile.GetComponent<TileGrid>().GetID().ToString();

                tile.SetActive(true);

                tileGrieds.Add(tile.GetComponent<TileGrid>());
            }
        }
        
        for (int i = 0; i < 2; i++)
        {
            float positionX = ((gridData.GetSize().x - 1) * gridData.GetSpace()) / 2;
            float positionZ = (((gridData.GetSize().y - 1) / 2) * gridData.GetSpace()) / 2;
            float scaleX = gridData.GetTileSize().x * gridData.GetSize().x;
            float scaleZ = gridData.GetTileSize().y * (gridData.GetSize().y / 2);
            GameObject gridSel = Instantiate(gridSelectionView, transform);
            gridSel.transform.localScale = new Vector3(scaleX + 0.1f, 1, scaleZ);
            gridSel.transform.localPosition = new Vector3(positionX, 1, (positionZ + (((gridData.GetSize().y / 2) * gridData.GetSpace()) * i) - 1));
            
            if(i % 2 == 0)
                gridSel.GetComponent<GridView>().Setup(TileType.Player);
            else
                gridSel.GetComponent<GridView>().Setup(TileType.Enemy);

            gridSel.GetComponent<GridView>().gameObject.SetActive(false);
            
            gridView[i] = gridSel.GetComponent<GridView>();
        }

        foreach (TileGrid tile in tileGrieds)
        {
            tile.SetNeighbornNode(gridData.GetSize().y, gridData.GetSize().x);
        }
    }

    public TileGrid GetNeigthbornTileEmpty(TileGrid tileStart, int team)
    {

        List<TileGrid> tiles = new List<TileGrid>(tileStart.GetNeighborn());
        List<TileGrid> tilesNeighborn = new List<TileGrid>(tiles);
        List<TileGrid> removeTiles = new List<TileGrid>();

        List<TileGrid> tilesControl = new List<TileGrid>(tileGrieds);


        while (tilesControl.Count != 0)
        {
            foreach (TileGrid tile in tiles)
            {
                if (!tile.GetOccupated() && tile.CorrectTile(team))
                {
                    return tile;
                }

                removeTiles.Add(tile);
                tilesNeighborn.AddRange(tile.GetNeighborn());


                if (tilesControl.Contains(tile))
                    tilesControl.Remove(tile);
            }

            foreach (TileGrid tile in removeTiles)
            {
                if (tilesNeighborn.Contains(tile))
                    tilesNeighborn.Remove(tile);
            }

            tiles = new List<TileGrid>(tilesNeighborn);
        }

        return null;
    }

    public bool ControlTileEmpty(int team)
    {

        foreach (TileGrid tile in tileGrieds)
        {
            if (!tile.GetOccupated() && tile.CorrectTile(team))
            {
                return true;
            }
        }
        return false;
    }

    public TileGrid GetTileBasedonPosition(Vector3 localPosition)
    {
        foreach (TileGrid tile in tileGrieds)
        {
            if (Vector3.SqrMagnitude(tile.transform.localPosition - localPosition) < 0.1f )
            {
                return tile;
            }
        }

        return null;
    }
    public List<TileGrid> GetTileRange(int team)
    {
        List<TileGrid> tileReturn = new List<TileGrid>();
        int total = (gridData.GetSize().y * gridData.GetSize().x);
        int metaCamp = total / 4;

        foreach (TileGrid tile in tileGrieds)
        {
            if (!tile.GetOccupated() && tile.CorrectTile(team))
            {
                if (team % 2 != 0 && tile.GetID() <= metaCamp)
                {
                        tileReturn.Add(tile);
                }
                else if (team % 2 == 0 && tile.GetID() > total - metaCamp)
                {
                        tileReturn.Add(tile);
                }
            }           
        }

        return tileReturn;
        
    }

    public List<TileGrid> GetTileMelee(int team)
    {
        List<TileGrid> tileReturn = new List<TileGrid>();
        int total = (gridData.GetSize().y * gridData.GetSize().x);
        int metaCamp = total / 4;

        foreach (TileGrid tile in tileGrieds)
        {
            if (!tile.GetOccupated() && tile.CorrectTile(team))
            {
                if (team % 2 != 0 && tile.GetID() > metaCamp)
                {
                    tileReturn.Add(tile);
                }
                else if (team % 2 == 0 && tile.GetID() <= total - metaCamp)
                {
                    tileReturn.Add(tile);
                }
            }
        }

        return tileReturn;

    }

    public TileGrid GetCenterCamp(int team)
    {
        int metaY = (gridData.GetSize().y / 4 * gridData.GetSize().x);
        int total = gridData.GetSize().y * gridData.GetSize().x;
        int metaX = (gridData.GetSize().x - 1) / 2;

        if (team % 2 != 0)
            return GetTile(metaY - metaX);
        else
            return GetTile((total - metaY) + metaX + 1);

    }

    public TileGrid GetTile(int id)
    {
        foreach (TileGrid tile in GetComponentsInChildren<TileGrid>())
        {
            if (tile.GetID() == id)
                return tile;
        }

        return null;
    }

    public void SetAllSelectionTile(TileSelection tileSelection)
    {
        foreach (TileGrid tile in tileGrieds)
        {
            tile.SelectTile(tileSelection);
        }

        ClearTileInvoke();
    }
}
