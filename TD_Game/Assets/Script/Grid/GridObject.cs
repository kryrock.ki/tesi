using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGridData", menuName = "GridSystem/Grid", order = 0)]
public class GridObject : ScriptableObject
{
    
    [SerializeField] protected int y;
    [SerializeField] protected int x;
    [SerializeField] protected float space;
    [SerializeField] protected Vector3 origin;
    [SerializeField] protected Vector2 tile_size;
    [SerializeField] protected GameObject tileAsset;

    public Vector2Int GetSize() => new Vector2Int(x, y);
    public float GetSpace() => space;
    public Vector3 GetOrigin() => origin;
    public Vector2 GetTileSize() => tile_size;
    public GameObject GetTile() => tileAsset;

}
