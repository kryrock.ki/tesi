using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR

[CustomEditor(typeof(GridManager))]
public class EditorGrid : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        //DrawDefaultInspector();

        GridManager gm = (GridManager)target;

        if (GUILayout.Button("Create Grid"))
        {
            
            gm.CreateGrid();
        }
    }
}

#endif