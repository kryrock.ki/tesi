using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridView : MonoBehaviour
{
    public TileType tileType;
    public Material materialEnemy;
    public Material materialPlayer;

    private Dictionary<TypeHeroControl, TileType> gridViewDict = new Dictionary<TypeHeroControl, TileType>();

    public void Setup(TileType tileType)
    {
        this.tileType = tileType;

        gridViewDict.Add(TypeHeroControl.Player, tileType);
        gridViewDict.Add(TypeHeroControl.CPU, tileType);

        
    }

    public void IsActive(TypeHeroControl typeHero = TypeHeroControl.None)
    {
        TileType type;

        switch (typeHero)
        {
            case TypeHeroControl.Player:
                if ((gridViewDict.TryGetValue(TypeHeroControl.Player, out type)))
                    SetControl(type);

                break;
            case TypeHeroControl.CPU:
                if ((gridViewDict.TryGetValue(TypeHeroControl.CPU, out type)))
                    SetControl(type);
                break;


        }
    }

    private void SetControl(TileType tileType)
    {
        switch (tileType)
        {
            
            case TileType.Player:
                GetComponentInChildren<MeshRenderer>().material = materialPlayer;
                break;
            case TileType.Enemy:
                GetComponentInChildren<MeshRenderer>().material = materialEnemy;
                break;

        }
    }
}
