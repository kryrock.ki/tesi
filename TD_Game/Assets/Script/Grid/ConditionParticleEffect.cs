using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ConditionParticleEffect : IConditionParticle
{
    public TypeEffect typeEffect;
    public ParticleSystem effectobject;

    public void ActiveTypeCondition(TypeCondition typeEffect)
    {
        return;
    }

    public void ActiveTypeEffect(TypeEffect typeEffect)
    {
        if (this.typeEffect == typeEffect)
        {
            effectobject.gameObject.SetActive(true);
            effectobject.Play();
        }
        else
        {
            effectobject.Clear();
            effectobject.gameObject.SetActive(false);
        }
    }
}
[System.Serializable]
public enum TypeEffect
{
    None,
    Dirt,
    Poison,
    Fire
}