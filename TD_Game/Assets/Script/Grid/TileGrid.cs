using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileGrid : MonoBehaviour
{
    private Vector2 size;
    [SerializeField] private int id = -1;
    [SerializeField] private TileType tileType;
    [SerializeField] private TileSelection tileSelection;
    public TileSelection GetTileSelection() { return tileSelection; }
    [SerializeField] private bool occupated;
    [SerializeField] private HeroMonster hero;

    [SerializeField] private MeshRenderer grassTile;
    [SerializeField] private Color grassColor1;
    [SerializeField] private Color grassColor2;
    [SerializeField] private MeshRenderer meshPlane;
    [SerializeField] private Color colorBorderTile;
    [SerializeField] private Color colorEnterTile;
    [SerializeField] private Color ColorDamageBorderPlane;
    [SerializeField] private Color ColorDamageEnterPlane;
    [SerializeField] private Color ColorDamagePrimaryEnterPlane;
    [SerializeField] private Color ColorDamagePrimaryBorderPlane;

    [SerializeField] private Color colorSelectBorderPlane;
    [SerializeField] private Color colorSelectEnterPlane;
    [SerializeField] private TileGrid[] tileNeighbornNode;
    [SerializeField] private TileGrid[] tileNeighbornDiagonal;
    [SerializeField] private ConditionParticle tileEffect;
    public ConditionParticle GetConditionParticle() => tileEffect;


    private void Start()
    {
        GameManagerBattle.instance.endTurn.AddListener((int point) => SetOccupied(false, null)); 
    }

    public void Initialize(int id, Vector2 size, TileType tileType)
    {
        SelectTile(TileSelection.None);
        this.id = id;

        if (id % 2 == 0)
        {
            grassTile.material.color = grassColor2;
        }
        else
        {
            grassTile.material.color = grassColor1;
        }

        this.size = size;
        this.tileType = tileType;
    }

    public TileGrid[] GetNeighborn() => tileNeighbornNode;
    public TileGrid[] GetNeighbornDiagonal() => tileNeighbornDiagonal;
    public bool IsConteinerNeighbornTile(TileGrid tileTarget)
    {
        foreach (TileGrid tile in tileNeighbornNode)
        {
            if (tile == tileTarget)
                return true;
        }

        foreach (TileGrid tile in tileNeighbornDiagonal)
        {
            if (tile == tileTarget)
                return true;
        }

        return false;
    }
    public void SetOccupied(bool val, HeroMonster hero)
    {
        this.hero = hero;
        occupated = val;

        
    }
    public void SelectTile(TileSelection tileSelection)
    {
        switch(tileSelection)
        {
            case TileSelection.None:
                meshPlane.material.SetColor("_Color2", colorBorderTile);
                meshPlane.material.SetColor("_Color", colorEnterTile);
                break;
            case TileSelection.Select:
                meshPlane.material.color = Color.cyan;
                meshPlane.material.SetColor("_Color2", colorSelectBorderPlane);
                meshPlane.material.SetColor("_Color", colorSelectEnterPlane);
                break;
            case TileSelection.Damage:
                meshPlane.material.SetColor("_Color2", ColorDamageBorderPlane);
                meshPlane.material.SetColor("_Color", ColorDamageEnterPlane);
                break;
            case TileSelection.PrimaryDamage:
                meshPlane.material.SetColor("_Color2", ColorDamagePrimaryBorderPlane);
                meshPlane.material.SetColor("_Color", ColorDamagePrimaryEnterPlane);
                break;
        }

        this.tileSelection = tileSelection;
    }
    public HeroMonster GetHero() => hero;
    public bool GetOccupated() =>  occupated;
    public Vector2 GetSize() => size;
    public int GetID() => id;
    public TileType GetTileType() => tileType;
    public bool CorrectTile(int team)
    {
        if (team % 2 == 0)
        {
            if (tileType == TileType.Enemy)
                return true;
            return false;
        }
        else
        {
            if (tileType == TileType.Player)
                return true;
            return false;
        }
       // switch (typeHero)
       // {
       //     case TypeHeroControl.Player:
       //         
       //         return false;
       //     case TypeHeroControl.CPU:
       //
       //         return false;
       // }

    }
    public void SetNeighbornNode(int gridSizeY, int gridSizeX)
    {
        List<TileGrid> listTot = new List<TileGrid>();

        List<int> valueId = new List<int>();

        if (id % gridSizeX != 0)
            valueId.Add(id + 1);          
        if ((id - 1) % gridSizeX != 0)
            valueId.Add(id - 1);
        valueId.Add(id + gridSizeX);
        valueId.Add(id - gridSizeX);

        foreach (int id in valueId)
        {
            TileGrid tile = GetComponentInParent<GridManager>().GetTile(id);
            if(tile != null)
                listTot.Add(tile);
        }

        tileNeighbornNode = listTot.ToArray();

        SetNeighbornDiagonal(gridSizeX, gridSizeY);
    }

    public void SetNeighbornDiagonal(int gridSizeX, int gridSizeY)
    {
        List<TileGrid> listTot = new List<TileGrid>();

        List<int> valueId = new List<int>();

        if (id + gridSizeX <= gridSizeX * gridSizeY)
        {
            if (id % gridSizeX != 0)
                valueId.Add((id + 1) + gridSizeX);
            if ((id - 1) % gridSizeX != 0)
                valueId.Add((id - 1) + gridSizeX);
        }
        if (id - gridSizeX > 0)
        {
            if (id % gridSizeX != 0)
                valueId.Add((id + 1) - gridSizeX);
            if ((id - 1) % gridSizeX != 0)
                valueId.Add((id - 1) - gridSizeX);
        }

        foreach (int id in valueId)
        {
            TileGrid tile = GetComponentInParent<GridManager>().GetTile(id);
            if (tile != null)
                listTot.Add(tile);
        }

        tileNeighbornDiagonal = listTot.ToArray();
    }
}

[System.Serializable]
public enum TileType
{
    Player,
    Enemy
}

[System.Serializable]
public enum TileSelection
{
    None,
    Select,
    Damage,
    PrimaryDamage
}