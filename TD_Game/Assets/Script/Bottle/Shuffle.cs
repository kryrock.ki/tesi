using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shuffle : MonoBehaviour
{
    [SerializeField] private Text textShuffle;
    private Squad refTeam;
    private IEnumerator Start()
    {
        refTeam = GameManagerBattle.instance.tm.GetPlayerTeam();

        refTeam.changeShuffle.AddListener(SetTextShuffle);
        //textShuffle.transform.eulerAngles = new Vector3(Camera.main.transform.eulerAngles.x, transform.eulerAngles.y - transform.eulerAngles.y, 0);

        yield return new WaitForSecondsRealtime(0.1f);

        SetTextShuffle(refTeam.shuffleCount);

        
    }

    private void SetTextShuffle(int shuffle)
    {
        textShuffle.text = shuffle.ToString();
        //Debug.Log(gameObject.name);
    }
}
