using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bottle : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textBottleElisir;
    private Squad refTeam;
    private IEnumerator Start()
    {
        refTeam = GameManagerBattle.instance.tm.GetPlayerTeam();

        refTeam.changeElisir.AddListener(SetTextElisir);
        textBottleElisir.transform.eulerAngles = new Vector3(Camera.main.transform.eulerAngles.x, transform.eulerAngles.y - transform.eulerAngles.y, 0);

        yield return new WaitForSecondsRealtime(0.1f);

        SetTextElisir(refTeam.elisir);
        
    }

    private void SetTextElisir(int elisir)
    {
        textBottleElisir.text = elisir.ToString();
    }
}
